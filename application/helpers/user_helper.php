<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



if ( ! function_exists('user_favorite'))
{
    function user_favorite($userId)
    {
    	$CI = get_instance();
        $CI->load->model('favorite_m');

        $favorite = $CI->favorite_m->getFavorite(array('userId' => $userId, 'status' => true));

        echo sizeof($favorite);
    }
}

if(!function_exists('user_notes'))
{
    function user_notes($userId)
    {
        $CI = get_instance();
        $CI->load->model('note_m');

        $note = $CI->note_m->getNote(array('userId' => $userId));

        echo sizeof($note);
    }
}

if(!function_exists('get_user_ip'))
{
    function get_user_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
}

?>
