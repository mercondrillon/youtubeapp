<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('todayStock'))
{
	function todayStock($id = null)
	{
		if(is_null($id))
			return false;

		$CI = get_instance();
        $CI->load->model('productquantity_m');


     	$today = strtotime(12 . ':00:00');
		$item_quantity = $CI->productquantity_m->getQuantity(array('productId'=> $id),'createdAt desc');

		return $item_quantity[0]->quantity;
	}
}

?>