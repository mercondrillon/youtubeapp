<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Insta_m extends CI_Model {

    private $table_name;

    public function __construct()
    {
    	parent::__construct();
    	$this->table_name = "channel_insta";
    }

    

    public function addInsta($data)
    {
    	$this->db->insert($this->table_name,$data);
    	return $this->db->insert_id();
    }

    public function getInsta($where)
    {
        $this->db->select('*');
        $this->db->where($where);
        $this->db->from($this->table_name);
        $query = $this->db->get();
        return $query->result();
    }

    public function updateInsta($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update($this->table_name, $data); 
    }

    public function deleteInsta($id)
    {
        $this->db->where('id',$id);
        $this->db->delete($this->table_name);
    }
}
?>