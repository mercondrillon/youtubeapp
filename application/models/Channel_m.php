<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Channel_m extends CI_Model {

    private $table_name;

    public function __construct()
    {
    	parent::__construct();
    	$this->table_name = "channels";
    }

    public function getAllChannels($where = null,$order_by = null)
    {
	  	$this->db->select("*");
	    $this->db->from($this->table_name);
	    if(!is_null($where))
        {
            $this->db->where($where);
        }

        $this->db->where('is_archive',false);
        
        if(is_null($order_by))
            $this->db->order_by("subscriberCount", "desc");
        else
            $this->db->order_by($order_by['key'], $order_by['type']);


	    $query = $this->db->get();


        $result = $query->result();


        $resultNoDuplicate = [];
        $resultYid = [];

        foreach ($result as $index => $value) {
            if(!in_array($value->yId,$resultYid))
            {
                array_push($resultYid, $value->yId);
                array_push($resultNoDuplicate,$value);
            }
        }
    
	    return $resultNoDuplicate;
        
    }

    public function addChannel($data)
    {
    	$this->db->insert($this->table_name,$data);
    	return $this->db->insert_id();
    }

    public function getChannel($where = null)
    {
    	if($where == null)
    	{
    		return false;
    	}

    	$query = $this->db->get_where($this->table_name,$where);
        return $query->result();
    }

    public function updateChannel($data,$yId)
    {
        $this->db->where('yId', $yId);
        $this->db->update($this->table_name, $data);
    }

    public function searchChannels($search,$where = null,$order_by = null)
    {
        $this->db->like('title', $search);
        if(!is_null($where))
        {
            $this->db->where($where);
        }
         $this->db->where('is_archive',false);
        if(!is_null($order_by))
            $this->db->order_by($order_by['key'], $order_by['type']);

        $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function getChannelCountries()
    {
        $this->db->select('country');
        $this->db->distinct();
        $this->db->from($this->table_name);
        $this->db->where(array( 'country <>' => '' ));
        $query = $this->db->get();

        return $query->result();

    }

    public function searchFrom($data)
    {
        
        $this->db->select('*');
        $this->db->from($this->table_name." AS channel");
         $this->db->distinct('yId');
        //$this->db->distinct();

        if($data['channel-name'] != "")
        {
            $this->db->like('title', $data['channel-name']);
            
        }

        $this->db->where('is_archive',false);
        $this->db->limit($data['search-limit'],$data['search-offset']);
       
        if($data['channel-category'] != 'default')
        {
            $this->db->where('categoryId',$data['channel-category']);
        }
        if($data['channel-country'] != 'default')
        {
            $this->db->where('country',$data['channel-country']);
        }
        if((int) $data['channel-subscriber-max'] > 0 )
        {
            $this->db->where('subscriberCount <=',$data['channel-subscriber-max']);
        }
        if((int) $data['channel-subscriber-min'] > 0 )
        {
            $this->db->where('subscriberCount >=',$data['channel-subscriber-min']);
        }
        if((int) $data['channel-view-max'] > 0)
        {
            $this->db->where('ave_view <= '.$data['channel-view-max']);
        }

        if((int) $data['channel-view-min'] > 0)
        {
            $this->db->where('ave_view >= '.$data['channel-view-min']);
        }

        if($data['channel-sort'] != 'default')
        {
            $this->db->order_by($data['channel-sort'],$data['channel-sort-type']);
        }

        $query = $this->db->get();

        return $query->result();

    }
}
?>