<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Video_m extends CI_Model {

    private $table_name;

    public function __construct()
    {
    	parent::__construct();
    	$this->table_name = "channelVideos";
    }

    

    public function addChannelVideo($data)
    {
    	$this->db->insert($this->table_name,$data);
    	return $this->db->insert_id();
    }

    public function getVideoData($videoId)
    {
        $this->db->select('*');
        $this->db->where('id',$videoId);
        $this->db->from($this->table_name);
        $query = $this->db->get();
        return $query->result();
    }

    public function getChannelVideo($channelId,$sortBy = null,$sortAs = 'asc',$limit = null,$offest = null)
    {
        $this->db->select('*');
        $this->db->where('channelId',$channelId);

        if(!is_null($sortBy))
        {
            $this->db->order_by($sortBy,$sortAs);    
        }
        
        if(!is_null($limit))
        {
            $this->db->limit($limit,$offest);
        }

        $this->db->from($this->table_name);
        $query = $this->db->get();
        return $query->result();
    }

    public function deleteChannelVideos($channelId)
    {
        $this->db->where('channelId',$channelId);
        $this->db->delete($this->table_name);
    }  


    public function deleteDuplicate($channelId)
    {
        //$query = $this->db->get_where($this->table_name,array('channelId'));

    }

    public function searchFrom($data)
    {
        
        $this->db->select('*');
        $this->db->from($this->table_name);
        $this->db->distinct('yId');
        //$this->db->distinct();

        if($data['video-name'] != "")
        {
            $this->db->like('title', $data['video-name']);
            
        }

       
        $this->db->limit($data['search-limit'],$data['search-offset']);
       
       
        if((int) $data['video-view-max'] > 0)
        {
            $this->db->where('ave_view <= '.$data['video-view-max']);
        }

        if((int) $data['video-view-min'] > 0)
        {
            $this->db->where('ave_view >= '.$data['video-view-min']);
        }

        if($data['video-sort'] != 'default')
        {
            $this->db->order_by($data['video-sort'],$data['video-sort-type']);
        }

        $query = $this->db->get();

        return $query->result();

    }
}
?>