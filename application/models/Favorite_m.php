<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Favorite_m extends CI_Model {

    private $table_name;

    public function __construct()
    {
    	parent::__construct();
    	$this->table_name = "favorite";
    }

    

    public function addFavorite($data)
    {
    	$this->db->insert($this->table_name,$data);
    	return $this->db->insert_id();
    }

    public function getFavorite($where)
    {
        $this->db->select('*');
        $this->db->where($where);
        $this->db->from($this->table_name);
        $query = $this->db->get();
        return $query->result();
    }

    public function updateFavorite($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update($this->table_name, $data); 
    }
}
?>