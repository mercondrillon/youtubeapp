<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Productquantity_m extends CI_Model {

    private $table_name;

    public function __construct()
    {
    	parent::__construct();
    	$this->table_name = "product_quantity";
    }

    

    public function addQuantity($data)
    {
    	$this->db->insert($this->table_name,$data);
    	return $this->db->insert_id();
    }

    public function getQuantity($where = null,$orderby = null)
    {
        $this->db->select('*');
        
        if(!is_null($where))
            $this->db->where($where);

        if(is_null($orderby))
        {
            $this->db->order_by('createdAt asc');
        }else
        {
            $this->db->order_by($orderby);
        }
        $this->db->from($this->table_name);
        $query = $this->db->get();
        return $query->result();
    }

    public function updateQuantity($id)
    {
        $this->db->where('id', $id);
        $data = array('is_process' => true );
        $this->db->update($this->table_name, $data);
    }
}
?>