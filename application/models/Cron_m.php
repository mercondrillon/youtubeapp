<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron_m extends CI_Model {

    private $table_name;

    public function __construct()
    {
    	parent::__construct();
    	$this->table_name = "cron_process";
    }

    

    public function addCronJob($data)
    {
    	$this->db->insert($this->table_name,$data);
    	return $this->db->insert_id();
    }

    public function getCronJob($where)
    {
        $this->db->select('*');
        $this->db->where($where);
        $this->db->from($this->table_name);
        $query = $this->db->get();
        return $query->result();
    }

    public function updateCronJob($id)
    {
        $this->db->where('id', $id);
        $data = array('is_process' => true );
        $this->db->update($this->table_name, $data);
    }
}
?>