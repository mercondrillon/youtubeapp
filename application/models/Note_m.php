<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Note_m extends CI_Model {

    private $table_name;

    public function __construct()
    {
    	parent::__construct();
    	$this->table_name = "notes";
    }

    

    public function addNote($data)
    {
    	$this->db->insert($this->table_name,$data);
    	return $this->db->insert_id();
    }

    public function getNote($where)
    {
        $this->db->select('*');
        $this->db->where($where);
        $this->db->from($this->table_name);
        $query = $this->db->get();
        return $query->result();
    }

    public function updateNote($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update($this->table_name, $data); 
    }

    public function deleteNote($id)
    {
        $this->db->where('id',$id);
        $this->db->delete($this->table_name);
    }
}
?>