<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category_m extends CI_Model {

    private $table_name;

    public function __construct()
    {
    	parent::__construct();
    	$this->table_name = "category";
    }

    public function getCategory($where)
    {
        $query = $this->db->get_where($this->table_name,$where);
        return $query->result();
    }

    public function addCategory($data)
    {
        $this->db->insert($this->table_name,$data);
        return $this->db->insert_id();
    }

    public function getAllCategories()
    {
        $this->db->select("*");
        $this->db->from($this->table_name);
        $query = $this->db->get();

        $result = $query->result();

        return $result;


    }
}
?>