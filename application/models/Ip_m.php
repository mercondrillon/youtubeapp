<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ip_m extends CI_Model {

    private $table_name;

    public function __construct()
    {
    	parent::__construct();
    	$this->table_name = "ip_allow";
    }


    public function addIp($data)
    {
    	$this->db->insert($this->table_name,$data);
    	return $this->db->insert_id();
    }

    public function getIp($where = null)
    {
    	if($where == null)
    	{
    		return false;
    	}

    	$query = $this->db->get_where($this->table_name,$where);
        return $query->result();
    }

    public function updateIp($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update($this->table_name, $data);
    }

    public function getAllIp()
    {
        $this->db->select("*");
        $this->db->from($this->table_name);
        $query = $this->db->get();


        $result = $query->result();

        return $result;
    }

    function deleteIp($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table_name);
    }

   
}
?>