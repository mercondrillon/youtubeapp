<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Project_m extends CI_Model {

    private $table_name;

    public function __construct()
    {
    	parent::__construct();
    	$this->table_name = "projects";
    }

    

    public function addProject($data)
    {
    	$this->db->insert($this->table_name,$data);
    	return $this->db->insert_id();
    }

    public function getProject($where)
    {
        $this->db->select('*');
        $this->db->where($where);
        $this->db->from($this->table_name);
        $query = $this->db->get();
        return $query->result();
    }

    public function updateProject($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update($this->table_name, $data); 
    }

    public function deleteProject($id)
    {
        $this->db->where('id',$id);
        $this->db->delete($this->table_name);
    }
}
?>