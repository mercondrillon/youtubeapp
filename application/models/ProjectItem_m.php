<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ProjectItem_m extends CI_Model {

    private $table_name;

    public function __construct()
    {
    	parent::__construct();
    	$this->table_name = "project_item";
    }

    

    public function addItem($data)
    {
    	$this->db->insert($this->table_name,$data);
    	return $this->db->insert_id();
    }

    public function getItem($where)
    {
        $this->db->select('*');
        $this->db->where($where);
        $this->db->from($this->table_name);
        $query = $this->db->get();
        return $query->result();
    }

    public function updateItem($data,$where,$multiple = false)
    {
        if($multiple)
            $this->db->where($where);
        else
            $this->db->where('id', $where);
        
        $this->db->update($this->table_name, $data); 
    }

    public function deleteItem($id)
    {
        $this->db->where('id',$id);
        $this->db->delete($this->table_name);
    }

    public function shareItem($option)
    {
        $this->db->select('channelId');
        $this->db->from($this->table_name);
        $this->db->limit($option['limit'],$option['offset']);
        $this->db->where('projectId',$option['projectId']);

        $query = $this->db->get();

        return $query->result();

    }
}
?>