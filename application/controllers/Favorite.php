<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Favorite extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		set_auth('login');
		$this->load->helper('url');
		$this->load->model('channel_m');
		$this->load->model('video_m');
		$this->load->model('category_m');
		$this->load->model('favorite_m');

		//init pages
		$this->data['page_title'] = 'favorite';
		$this->output->set_template('default');
		
	}

	public function channel()
	{
		$this->output->unset_template();

		$channel = $_POST['channel'];
		$user = $_POST['user'];

		$favorite = $this->favorite_m->getFavorite(array('userId'=>$user,'channelId'=>$channel));

		if(sizeof($favorite) == 0)
		{
			$data = array(
				'userId' => $user,
				'channelId' => $channel,
				'status' => true,
			);

			$favorite_id = $this->favorite_m->addFavorite($data);
		}else
		{
			$data = array(
				'userId' => $user,
				'channelId' => $channel,
				'status' => !$favorite[0]->status,
			);

			$this->favorite_m->updateFavorite($data,$favorite[0]->id);
			$favorite_id = $favorite[0]->id;
		}

		$favorite = $this->favorite_m->getFavorite(array('id'=>$favorite_id));

		echo $favorite[0]->status? 'btn-primary':'btn-default';

		
	}


	public function favoiteList()
	{
		// $favorite_channel = [];
		$user = $this->session->userdata('user_data')[0];

		// $favorite = $this->favorite_m->getFavorite(array('userId' => $user->id, 'status' => true));

		// foreach ($favorite as $index => $fav) {
		// 	$channel = $this->channel_m->getChannel(array('id' => $fav->channelId));
		// 	$videos = $this->video_m->getChannelVideo($channel[0]->id,'viewCount','asc',20);
		// 	$video_total_views_average = 0;
		// 	if(sizeof($videos) != 0)
		// 	{
		// 		foreach ($videos as $video_index => $data) {
		// 			$video_total_views_average += $data->viewCount;
		// 		}

		// 		$video_total_views_average = $video_total_views_average / sizeof($videos);
		// 	}

		// 	$channel[0]->aveViews = $video_total_views_average;
		// 	// if($start_datandex == 3999)
		// 	array_push($favorite_channel, $channel[0]);
			
		// }


		// $this->data['favorite_channel'] = $favorite_channel;
		$this->data['user'] = $user;

		$this->load->js('assets/js/favorite/favorite.js');
		$this->load->view('favorite/list',$this->data);
	}

	
	
}

?>