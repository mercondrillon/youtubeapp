<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('user_m');
		$this->load->model('ip_m');
		$this->load->model('insta_m');
		$this->load->model('category_m');
		$this->load->model('channel_m');
		$this->load->model('projectItem_m');
		$this->load->model('video_m');
		$this->load->model('product_m');
		$this->load->model('productquantity_m');

		

		
	}

	
	public function import_bulk_channel($data_id)
	{
		$where = array(
			'id' => $data_id, 
		);
		$cron_job = $this->cron_m->getCronJob($where);


		if($cron_job[0]->is_process == "0")
		{
			$value  = json_decode($cron_job[0]->value);
		
			$import_in_project = false;

			if($value->project_id != "default")
				$import_in_project = true;

			$bulk_channels = $value->bulk_channels;

			foreach ($bulk_channels as $index => $url) {
	
				$channels = send_request('https://www.googleapis.com/youtube/v3/search?&part=snippet&maxResults=1&type=channel&q='.$url);

				if(property_exists($channels,'items'))
				{
					if(sizeof($channels->items) != 0)
					{
						
						$already_save = false;

						$channel_data_info = $this->channel_m->getChannel(array('yId' => $channels->items[0]->snippet->channelId));
						if(!empty($channel_data_info))
							$already_save = true;

						if(!$already_save)
						{

							if($import_in_project)
							{
								$channel_id = $this->addChannel($channels->items[0]->snippet->channelId,true);
								
								$data = array(
									'projectId' => $value->project_id,
									'channelId' => $channel_id, 
									'is_active' => true,
								);

								$new_project_item_id = $this->projectItem_m->addItem($data);

							}else
							{
								$this->addChannel($channels->items[0]->snippet->channelId);
							}
							
							
						}else
						{
							if($import_in_project)
							{
								
								$if_exist = $this->projectItem_m->getItem(array('projectId'=>$value->project_id,'channelId' => $channel_data_info[0]->id));
								

								if(sizeof($if_exist) == 0)
								{
									$data = array(
										'projectId' => $value->project_id,
										'channelId' => $channel_data_info[0]->id, 
										'is_active' => true,
									);

									$new_project_item_id = $this->projectItem_m->addItem($data);
								}else
								{
									echo "already add in the project.\n";
								}
							}
							echo "This channel is already exists in our database.\n\n";

						}

					}else
					{
						echo "No Information get from this url.\n";
					}
				}else
				{
					echo "No Information get from this url.\n";
				}
			}

			$cron_job = $this->cron_m->updateCronJob($data_id);

		}
	}

	public function addChannel($channelId = null,$csv = false)
	{
		

		if(is_null($channelId))
			$id = $_POST['channelId'];
		else
			$id = $channelId;

		$channel_data = send_request('https://www.googleapis.com/youtube/v3/channels?id='.$id.'&part=statistics,snippet');


		foreach ($channel_data->items as $item_key => $item_data) {
							    	
		    $insert_data = array(
		    	'yId' => $item_data->id,
		    	'title' => property_exists($item_data->snippet, 'title')? $item_data->snippet->title:'', 
		    	'description' => property_exists($item_data->snippet, 'description')? $item_data->snippet->description:'', 
		    	'publishedAt' => property_exists($item_data->snippet, 'publishedAt')? $item_data->snippet->publishedAt:'', 
		    	'thumbnails' => property_exists($item_data->snippet, 'thumbnails')? json_encode($item_data->snippet->thumbnails):'',
		    	'country' => property_exists($item_data->snippet, 'country')? $item_data->snippet->country:'', 
		    	'viewCount' => property_exists($item_data->statistics, 'viewCount')? $item_data->statistics->viewCount:'', 
		    	'commentCount' => property_exists($item_data->statistics, 'commentCount')? $item_data->statistics->commentCount:'', 
		    	'subscriberCount' => property_exists($item_data->statistics, 'subscriberCount')? $item_data->statistics->subscriberCount:'', 
		    	'videoCount' => property_exists($item_data->statistics, 'videoCount')? $item_data->statistics->videoCount:'', 
		    );

		    $channel_id = $this->channel_m->addChannel($insert_data);


		    $videos = send_request('https://www.googleapis.com/youtube/v3/search?channelId='.$id.'&part=snippet,id&order=date&maxResults=30&type=video');

		    foreach ($videos->items as $video_index => $video_data) {
							
				$video_info = send_request('https://www.googleapis.com/youtube/v3/videos?id='.$video_data->id->videoId.'&part=statistics,snippet');

				if(!property_exists($video_info, 'items'))
				{
					

				}else
				{


					$insert_data = array(
				    	'yId' => $video_data->id->videoId,
				    	'title' => property_exists($video_info->items[0]->snippet, 'title')? $video_info->items[0]->snippet->title:'',  
				    	'viewCount' => property_exists($video_info->items[0]->statistics, 'viewCount')? $video_info->items[0]->statistics->viewCount:'', 
				    	'channelId' => $channel_id, 
				    	'date' => property_exists($video_info->items[0]->snippet, 'publishedAt')? $video_info->items[0]->snippet->publishedAt:'', 
				    );

				    $videos_id = $this->video_m->addChannelVideo($insert_data);
				   
				    // sleep(2);
				}
			}
			 

			$category = [];
			$videos = $this->video_m->getChannelVideo($channel_id,'viewCount','asc',30);

			if(sizeof($videos) != 0)
			{

				foreach ($videos as $index => $video) {
					
					$video_info = send_request('https://www.googleapis.com/youtube/v3/videos?id='.$video->yId.'&part=statistics,snippet');
					
					array_push($category,$video_info->items[0]->snippet->categoryId);
				}

				$category_str = "";
				$ccounter= 0;
				$categoryNoDuplicate = array_unique($category);
				$count_index_arr = array();
				foreach ($categoryNoDuplicate as $cindex => $cat) {
					$count_dup = 0;
					foreach ($category as $dupindex => $dup) {
						if($cat == $dup)
						{
							$count_dup++;
						}
					}

					array_push($count_index_arr,[$cindex,$count_dup]);
					
				}

				$category_index = 0;
				$category_count = 0;
				foreach ($count_index_arr as $index => $cat) {
					if($category_count < $cat[1])
					{
						$category_count = $cat[1];
						$category_index = $cat[0];
					}
				}

				$cat_info = send_request('https://www.googleapis.com/youtube/v3/videoCategories?id='.$categoryNoDuplicate[$category_index].'&part=snippet');

				if(!is_null($cat_info->items[0]->snippet->title))
				{
					$category_data = $this->category_m->getCategory(array('name' => $cat_info->items[0]->snippet->title));
							
					if(sizeof($category_data) == 0)
					{
						$data = array(
							'name' => $cat_info->items[0]->snippet->title, 
						);
						$category_id = $this->category_m->addCategory($data);
					}else
					{
						$category_id = $category_data[0]->id;
					}

		
					$update_data = array( 
				    	'categoryId' => $category_id, 
				    );

				    $this->channel_m->updateChannel($update_data,$id);
				}
			    
			}  

			$videos_list_top_20 = $this->video_m->getChannelVideo($channel_id,'viewCount','asc',20);
					
			if(sizeof($videos_list_top_20) != 0)
			{
				$channel_yid = $this->channel_m->getChannel(array('id' => $channel_id))[0]->yId;
				$ave_view = 0;
				
				foreach ($videos_list_top_20 as $video_index => $data) {
					$ave_view += $data->viewCount;
				}

				$ave_view = floor($ave_view / sizeof($videos_list_top_20));

		
				$update_data = array( 
			    	'ave_view' => $ave_view, 
			    );
				
			    $this->channel_m->updateChannel($update_data,$channel_yid);
			   

			} 


	    }


	   	$response = array('status' => true,'message' => 'Channel Successfully Added.', 'channelId' => $channel_id );

	   	if(is_null($channelId))
	   		echo json_encode($response);
	   	else if($csv)
	   		return $channel_id;
	}

	public function csvFile($data_id)
	{
		$where = array(
			'id' => $data_id, 
		);
		$cron_job = $this->cron_m->getCronJob($where);

	

		if($cron_job[0]->is_process == "0")
		{
			$value = json_decode($cron_job[0]->value);
			$items = $value->items;
			
			foreach ($items as $index => $data) {
				if($index > 0)
				{
					$already_save = false;

					$channels = send_request('https://www.googleapis.com/youtube/v3/search?&part=snippet&maxResults=1&type=channel&q='.$data[0]);

					$channel_data_info = $this->channel_m->getChannel(array('yId' => $channels->items[0]->snippet->channelId));

					if(!empty($channel_data_info))
						$already_save = true;


					if(!$already_save)
					{
						
						$channel_id = $this->addChannel($channels->items[0]->snippet->channelId,true);
						
						$channel_data_info = $this->channel_m->getChannel(array('id' => $channel_id));

					}
			

					$update_data = array(
						'email' => $data[1], 
						'cost' => (float) filter_var( $data[2], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION ), 
					);

					$this->channel_m->updateChannel($update_data,$channel_data_info[0]->yId);
					echo "channel ".$channel_data_info[0]->title."\n";
				}

				
			}
			$cron_job = $this->cron_m->updateCronJob($data_id);
		}

		
	}

	public function save_channel_data($data_id)
	{
		$where = array(
			'id' => $data_id, 
		);
		$cron_job = $this->cron_m->getCronJob($where);

		

		if($cron_job[0]->is_process == "0")
		{
			
			$channels = $this->channel_m->getAllChannels();

			foreach ($channels as $index => $channel_value) {
				$cname = [$channel_value->title,$channel_value->yId];

				$channel_data_info = $this->channel_m->getChannel(array('title' => $cname[1]));

				if(sizeof($channel_data_info) == 0)
				{
					$update_cahannel_status = false;
					
					// sleep(1);
					$channels = send_request('https://www.googleapis.com/youtube/v3/search?&part=snippet&maxResults=1&type=channel&q='.$cname[1]);
					
					if(is_null($channels) || property_exists($channels, 'error'))
					{
						echo "first atemp failed...\n";
						echo "Getting information: Second atemp.\n";
						// sleep(1);
						$channels = send_request('https://www.googleapis.com/youtube/v3/search?&part=snippet&maxResults=1&type=channel&q='.$cname[0]);
					}

					if(is_null($channels))
					{
						echo "second channels request is null";
						echo "<pre>";
			    		print_r($channels);
			    		echo "</pre>";
						exit();
					}

					if(!property_exists($channels, 'error'))
					{
						if(property_exists($channels->items[0]->id, 'channelId'))
						{
							$channel_data_info = $this->channel_m->getChannel(array('yId' => $channels->items[0]->id->channelId));
						}else
						{
							$channel_data_info = $this->channel_m->getChannel(array('yId' => $channels->items[0]->id->playlistId));
						}

						if(sizeof($channel_data_info) == 0)
						{
							$update_cahannel_status = false;
							echo "<b>".($index + 1).". New Channel</b>\n";
							echo "<b>Channel Name:</b> ".$cname[1]."\n";
							echo "Getting information: first atemp.\n";


							echo "getting Information Success!!!\n";
							echo "getting channel statistic.\n";
							// sleep(1);
							if(property_exists($channels->items[0]->id, 'channelId'))
							{
								$channel_data = send_request('https://www.googleapis.com/youtube/v3/channels?id='.$channels->items[0]->id->channelId.'&part=statistics,snippet');
							}else
							{
								$channel_data = send_request('https://www.googleapis.com/youtube/v3/playlists?id='.$channels->items[0]->id->playlistId.'&part=statistics,snippet');
							}
					    	

					    	if(!is_object($channel_data) || property_exists($channel_data, 'error'))
					    	{
					    		echo "not object or emty Items";
					    		echo "<pre>";
					    		print_r($channel_data);
					    		echo "</pre>";
					    		
					    	}else
					    	{		    	
							    foreach ($channel_data->items as $item_key => $item_data) {
							    	
								    $insert_data = array(
								    	'yId' => $item_data->id,
								    	'title' => property_exists($item_data->snippet, 'title')? $item_data->snippet->title:'', 
								    	'description' => property_exists($item_data->snippet, 'description')? $item_data->snippet->description:'', 
								    	'publishedAt' => property_exists($item_data->snippet, 'publishedAt')? $item_data->snippet->publishedAt:'', 
								    	'thumbnails' => property_exists($item_data->snippet, 'thumbnails')? json_encode($item_data->snippet->thumbnails):'',
								    	'country' => property_exists($item_data->snippet, 'country')? $item_data->snippet->country:'', 
								    	'viewCount' => property_exists($item_data->statistics, 'viewCount')? $item_data->statistics->viewCount:'', 
								    	'commentCount' => property_exists($item_data->statistics, 'commentCount')? $item_data->statistics->commentCount:'', 
								    	'subscriberCount' => property_exists($item_data->statistics, 'subscriberCount')? $item_data->statistics->subscriberCount:'', 
								    	'videoCount' => property_exists($item_data->statistics, 'videoCount')? $item_data->statistics->videoCount:'', 
								    );

								    $channel_id = $this->channel_m->addChannel($insert_data);
									    
								    echo "New Channel Successfully Added.\n\n";
							    }
							}
						}else
						{
							$update_cahannel_status = true;
						}
					    
					}else
					{
						echo "Request for the information of ".$cname[1]." return error.\n";

						echo "<pre>";
						print_r($channels);
						echo "</pre>";
					}
				}else
				{
					$update_cahannel_status = true;
				}

				if($update_cahannel_status)
				{
					echo "<b style='color:blue;' >".($index + 1).". Update Channel Name:</b> ".$channel_data_info[0]->title."\n";
					echo "getting channel statistic.\n";
					// sleep(1);
					
					$channel_data = send_request('https://www.googleapis.com/youtube/v3/channels?id='.$channel_data_info[0]->yId.'&part=statistics,snippet');
					
			    	
					
			    	if(!is_object($channel_data))
			    	{
			    		echo "not object";
			    		echo "<pre>";
			    		print_r($channel_data);
			    		echo "</pre>";
			    		exit();
			    	}
			    	
			    	
				    foreach ($channel_data->items as $item_key => $item_data) {
				    	
					    $update_data = array( 
					    	'description' => property_exists($item_data->snippet, 'description')? $item_data->snippet->description:'', 
					    	'publishedAt' => property_exists($item_data->snippet, 'publishedAt')? $item_data->snippet->publishedAt:'', 
					    	'thumbnails' => property_exists($item_data->snippet, 'thumbnails')? json_encode($item_data->snippet->thumbnails):'', 
					    	'country' => property_exists($item_data->snippet, 'country')? $item_data->snippet->country:'', 
					    	'viewCount' => property_exists($item_data->statistics, 'viewCount')? $item_data->statistics->viewCount:'', 
					    	'commentCount' => property_exists($item_data->statistics, 'commentCount')? $item_data->statistics->commentCount:'', 
					    	'subscriberCount' => property_exists($item_data->statistics, 'subscriberCount')? $item_data->statistics->subscriberCount:'', 
					    	'videoCount' => property_exists($item_data->statistics, 'videoCount')? $item_data->statistics->videoCount:'', 
					    );

					    $this->channel_m->updateChannel($update_data,$channel_data_info[0]->yId);
						    

				    }
				    $channel_id = $channel_data_info[0]->id;
				    echo "<b>".$channel_data_info[0]->title."</b> Successfully updated.\n\n";
					
				}
				
				$videos = $this->video_m->getChannelVideo($channel_id,'viewCount','asc',30);

				//if(sizeof($videos) == 0 || sizeof($videos) < 30)
				if(true)
				{
					echo "delete existing video in database".
					$this->video_m->deleteChannelVideos($channel_id);
					echo "Get 30 latest videos.\n";
					// sleep(1);
					$channel_data_info = $this->channel_m->getChannel(array('id' => $channel_id));
					
					$videos = send_request('https://www.googleapis.com/youtube/v3/search?channelId='.$channel_data_info[0]->yId.'&part=snippet,id&order=date&maxResults=30&type=video');
					
					if(sizeof($videos->items) == 0)
					{
						echo "Nothing to Add...\n";
					}else
					{

						foreach ($videos->items as $video_index => $video_data) {
							
							$video_info = send_request('https://www.googleapis.com/youtube/v3/videos?id='.$video_data->id->videoId.'&part=statistics,snippet');

							if(!property_exists($video_info, 'items'))
							{
								echo "this video has error.";
								echo "<pre>";
								var_dump($video_info);
								echo "</pre>";

							}else
							{


								$insert_data = array(
							    	'yId' => $video_info->items[0]->id,
							    	'title' => property_exists($video_info->items[0]->snippet, 'title')? $video_info->items[0]->snippet->title:'',  
							    	'viewCount' => property_exists($video_info->items[0]->statistics, 'viewCount')? $video_info->items[0]->statistics->viewCount:'', 
							    	'channelId' => $channel_data_info[0]->id, 
							    	'date' => property_exists($video_info->items[0]->snippet, 'publishedAt')? $video_info->items[0]->snippet->publishedAt:'', 
							    );

							    $channel_video_id = $this->video_m->addChannelVideo($insert_data);
							    echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Add video title:<b>".$video_info->items[0]->snippet->title."</b>\n";
							    // sleep(2);
							}
						}
					}
				}

				$channel_data_info = $this->channel_m->getChannel(array('id' => $channel_id));

				if(empty($channel_data_info[0]->category))
				{
					$category = [];
					$videos = $this->video_m->getChannelVideo($channel_id,'viewCount','asc',30);
					if(sizeof($videos) != 0)
					{

						foreach ($videos as $index => $video) {
							
							$video_info = send_request('https://www.googleapis.com/youtube/v3/videos?id='.$video->yId.'&part=statistics,snippet');
							array_push($category,$video_info->items[0]->snippet->categoryId);
						}

						$category_str = "";
						$ccounter= 0;
						$categoryNoDuplicate = array_unique($category);
						$count_index_arr = array();
						foreach ($categoryNoDuplicate as $cindex => $cat) {
							$count_dup = 0;
							foreach ($category as $dupindex => $dup) {
								if($cat == $dup)
								{
									$count_dup++;
								}
							}

							array_push($count_index_arr,[$cindex,$count_dup]);
							
						}

						$category_index = 0;
						$category_count = 0;
						foreach ($count_index_arr as $index => $cat) {
							if($category_count < $cat[1])
							{
								$category_count = $cat[1];
								$category_index = $cat[0];
							}
						}

						$cat_info = send_request('https://www.googleapis.com/youtube/v3/videoCategories?id='.$categoryNoDuplicate[$category_index].'&part=snippet');
						if(!is_null($cat_info->items[0]->snippet->title))
						{
							$category_data = $this->category_m->getCategory(array('name' => $cat_info->items[0]->snippet->title));
							
							if(sizeof($category_data) == 0)
							{
								$data = array(
									'name' => $cat_info->items[0]->snippet->title, 
								);
								$category_id = $this->category_m->addCategory($data);
							}else
							{
								$category_id = $category_data[0]->id;
							}

				
							$update_data = array( 
						    	'categoryId' => $category_id, 
						    );

						    $this->channel_m->updateChannel($update_data,$channel_data_info[0]->yId);
						}
					    
					    $channel_data_info = $this->channel_m->getChannel(array('id' => $channel_id));
					}
					
				}

				$videos_list_top_20 = $this->video_m->getChannelVideo($channel_id,'viewCount','asc',20);
				
				if(sizeof($videos_list_top_20) != 0)
				{
					$ave_view = 0;

					foreach ($videos_list_top_20 as $video_index => $data) {
						$ave_view += $data->viewCount;
					}

					$ave_view = floor($ave_view / sizeof($videos_list_top_20));

					$update_data = array( 
				    	'ave_view' => $ave_view, 
				    );

				    $this->channel_m->updateChannel($update_data,$channel_id);

				}

				echo "Done...\n";
				echo "<hr/>\n\n";
			}
		}

	}

	public function data_scrape_insta($data_id)
	{
		$where = array(
			'id' => $data_id, 
		);
		$cron_job = $this->cron_m->getCronJob($where);


		if($cron_job[0]->is_process == "0")
		{
			ini_set('max_execution_time', 30000);

			$is_null = false;
			$page = 95;
			$counter_index = 0;
			while (!$is_null) {

				$fields = array(
					'q'=>'',
					'platforms'=>array('instagram'),
					'min_followers'=>'',
					'max_followers'=>'',
					'job_type'=>'',
					'min_budget'=>'',
					'max_budget'=>'',
					'gender'=>'50',
					'sort_by'=>'subscribers_high',
					'campaign_id'=>'',
					'list_id'=>''
				);

				$field_string = http_build_query($fields);

				$ch = curl_init();

				curl_setopt($ch, CURLOPT_URL,"https://socialbluebook.com/django/api/v1/search/?page=".$page);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS,$field_string);

				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

				$server_output = curl_exec ($ch);

				curl_close ($ch);

				$result = json_decode($server_output);

				
				if(!is_null($result))
				{

					foreach ($result->results as $index => $data) {

						$get_info = false;
						// exit();
						//if($counter_index >= 1912){
							foreach ($data->additional_platforms as $index_plat => $platform) {
								
								if($data->type == "instagram")
								{
									
									if($platform->type == "youtube")
									{
										$get_info = true;
										$youtube_url = $platform->platform_link;
										$username = $data->username;
										$followers = $data->subscribers;
										$likes = (int) $data->metrics=="None"? 0:$data->metrics;
										$instagram_ulr = $data->platform_link;

									}
								}else if($data->type == "youtube")
								{
									
									if($platform->type == "instagram")
									{
										$get_info = true;
										$youtube_url = $data->platform_link;
										$username = $platform->username;
										$followers = $platform->subscribers;
										$likes = (int) $platform->metrics=="None"? 0:$data->metrics;
										$instagram_ulr = $platform->platform_link;


										
									}
								}
								
								
							}

							if($get_info)
							{
								$data_arr = array(
									'youtube_url' => $youtube_url, 
									'username' => $username, 
									'followers' => $followers, 
									'likes' => $likes, 
									'instagram_ulr' => $instagram_ulr, 
								);

								$this->save_data($data_arr);
							}
						//}

						$counter_index++;
					}
					
					$page++;
				}else
				{
					$is_null = true;
				}
			}

			$cron_job = $this->cron_m->updateCronJob($data_id);
		}

	}

	public function save_data($data_arr)
	{
		echo "<pre>";
		var_dump($data_arr);
		echo "</pre>";
		$serach_channel = explode('/', $data_arr['youtube_url']);
		
		$channels = send_request('https://www.googleapis.com/youtube/v3/search?q='.$serach_channel[sizeof($serach_channel)-1].'&part=id&order=date&maxResults=1&type=channel');

		if(sizeof($channels->items) == 0)
		{
			$channels = send_request('https://www.googleapis.com/youtube/v3/search?channelId='.$serach_channel[sizeof($serach_channel)-1].'&part=id&order=date&maxResults=1&type=channel');
		}

		if(sizeof($channels->items) != 0)
		{
			$channel_id = $channels->items[0]->id->channelId;
			
			$channel_db_data = $this->channel_m->getChannel(array('yId'=>$channel_id));

			
			if(sizeof($channel_db_data) == 0)
			{
				$channel_db_id = $this->addChannel($channel_id,true);		
			}else
			{
				$channel_db_id = $channel_db_data[0]->id;
			}


			$check_insta = $this->insta_m->getInsta(array('username'=> $data_arr['username']));

			if(sizeof($check_insta) == 0)
			{
				$insert_data_insta = array(
					'username' => $data_arr['username'], 
					'followers' => $data_arr['followers'], 
					'likes' => $data_arr['likes'], 
					'channelId' => $channel_db_id, 
				);
				
				$insta_id = $this->insta_m->addInsta($insert_data_insta);
				var_dump($insta_id);
				echo "insert insta";
			}else
			{
				$insert_data_insta = array(
					'followers' => $data_arr['followers'], 
					'likes' => $data_arr['likes'], 
					'channelId' => $channel_db_id, 
				);

				$this->insta_m->updateInsta($insert_data_insta,$check_insta[0]->id);
				echo "update insta";
			}

			
			
		}
		

		echo "<br/><br/>";
	}

	public function save_bulk_product($data_id)
	{
		$where = array(
			'id' => $data_id, 
		);
		$cron_job = $this->cron_m->getCronJob($where);

	

		if($cron_job[0]->is_process == "0")
		{
			$value = json_decode($cron_job[0]->value);
			

			foreach ($value as $index => $asin) {
				
				$item_data = $this->product_m->getProduct(array('asin'=> $asin,'userId' => $cron_job[0]->userId));
			
				if(sizeof($item_data) == 0)
				{	
					$add_to_database = true;

					//get quantity
					$qunatity_error = true;
					while ($qunatity_error) {
						$quantity_param = array(
						    "Service" => "AWSECommerceService",
						    "Operation" => "CartCreate",
						    "AWSAccessKeyId" => "AKIAJC6CZCPCDS6BUGMA",
						    "AssociateTag" => "asdasd1232",
						    "Item.1.ASIN" => $asin,
						    "Item.1.Quantity" => 999
						);

						$quantity = amazom_api($quantity_param);

						if(property_exists($quantity,'Error') || is_null($quantity))
						{
							$qunatity_error = true;
						}else
						{
							$qunatity_error = false;
						}
					}

					$item = $quantity->Cart->CartItems->CartItem;

					
					//get error code
					$error_get_to_cart = false;
					if(property_exists($quantity->Cart->Request,'Errors'))
					{
						$error = $quantity->Cart->Request->Errors->Error;
						
						if($error->Code != "AWS.ECommerceService.InvalidQuantity")
						{
							$error_get_to_cart = true;
						}
					}
					

					//get image
					$img_param_error = true;
					while ($img_param_error) {
						$img_param = array(
						    "Service" => "AWSECommerceService",
						    "Operation" => "ItemLookup",
						    "AWSAccessKeyId" => "AKIAJC6CZCPCDS6BUGMA",
						    "AssociateTag" => "asdasd1232",
						    "ResponseGroup" => "Images",
						    "IdType" => 'ASIN',
						    "ItemId" => $asin,
						    
						);
						
						$img = amazom_api($img_param);

						if(property_exists($img,'Error') || is_null($img))
						{
							$img_param_error = true;
						}else
						{
							$img_param_error = false;
						}
					}

					
					$img_val = $img->Items->Item->MediumImage->URL;

					if(is_null($img_val))
					{
						$img_val = "none";
					}

					//get item data
					$get_item_data_error = true;
					while ($get_item_data_error) {
						$get_item_date = array(
						    "Service" => "AWSECommerceService",
						    "Operation" => "ItemLookup",
						    "AWSAccessKeyId" => "AKIAJC6CZCPCDS6BUGMA",
						    "AssociateTag" => "asdasd1232",
						    "IdType" => 'ASIN',
						    "ItemId" => $asin,
						    
						);
						
						$lookup = amazom_api($get_item_date);

						if(property_exists($lookup,'Error') || is_null($lookup))
						{
							$get_item_data_error = true;
						}else
						{
							$get_item_data_error = false;
						}
					}

					$lookup_item = $lookup->Items->Item;
					
					if(property_exists($lookup->Items->Request, 'Errors'))
					{
						if($error_get_to_cart)
						{
							$add_to_database = false;
						}
						
					}

					$result_error_detail_url = true;
					while ($result_error_detail_url) {

						$params_detail_url = array(
						    "Service" => "AWSECommerceService",
						    "Operation" => "ItemLookup",
						    "AWSAccessKeyId" => "AKIAJC6CZCPCDS6BUGMA",
						    "AssociateTag" => "asdasd1232",
						    "ResponseGroup" => 'ItemAttributes',
						    "IdType" => 'ASIN',
						    "ItemId" => $asin,
						    
						);

						$detail_url = amazom_api($params_detail_url);
						if(property_exists($detail_url,'Error'))
						{
							$result_error_detail_url = true;
						}else
						{
							$result_error_detail_url = false;
						}
					}

					$detailUrl = json_decode(json_encode($detail_url->Items->Item->DetailPageURL),true)[0];
					
					if($add_to_database)
					{
						if($error_get_to_cart)
						{
							$sellerNickname = "none";
							$price = "none";
							$quantity_value = "error";
							$asin_new_item = json_decode(json_encode($lookup_item->ASIN),true)[0];
							$title_new_item = json_decode(json_encode($lookup_item->ItemAttributes->Title),true)[0];
						}else
						{
							$sellerNickname = json_decode(json_encode($item->SellerNickname),true)[0];
							$price = json_encode($item->Price);
							$quantity_value = json_decode(json_encode($item->Quantity),true)[0];
							$asin_new_item = json_decode(json_encode($item->ASIN),true)[0];
							$title_new_item = json_decode(json_encode($item->Title),true)[0];
						}

						//set the item data
						$item_data = array(
							'asin' => $asin_new_item, 
							'userId' => $cron_job[0]->userId,
							'sellerNickname' => $sellerNickname, 
							'title' => $title_new_item, 
							'price' => $price, 
							'img' => json_decode(json_encode($img_val),true)[0], 
							'detailUrl' => $detailUrl,
							'is_update' => true, 
							'is_track' => true, 
						);
						
						$item_id = $this->product_m->addProduct($item_data);

						$item_quantity = array(
							'productId' => $item_id,
							'quantity' => $quantity_value,
							'createdAt' => strtotime(12 . ':00:00'),
						);

						$qunatity_id = $this->productquantity_m->addQuantity($item_quantity);
					}
				}
					
				echo "add this product\n";
				
			}

			$cron_job = $this->cron_m->updateCronJob($data_id);
		}
	}
	
}

?>