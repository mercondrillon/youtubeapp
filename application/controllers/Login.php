<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		is_login();
		$this->load->helper('url');
		$this->load->model('user_m');
		$this->load->model('ip_m');

		//init pages
		$this->data['page_title'] = 'Login';
		$this->data['no_nav'] = true;
		$this->output->set_template('default');

		
	}

	public function index()
	{

		$google = new Google();

		$client = new Google_Client();
		$client->setClientId($google->clientId());
		$client->setClientSecret($google->clientSecret());
		$client->setRedirectUri($google->redirectUri());
		$client->addScope("email");
		$client->addScope("profile");

		$authUrl = $client->createAuthUrl();

		$this->data['authUrl'] = $authUrl;

		$this->load->css('assets/css/login/login.css');

		$this->load->view('login/index',$this->data);
	}

	public function g_data()
	{
		$data = array(
			'IP' => get_user_ip(), 
		);
		$ip = $this->ip_m->getIp($data);

		if(sizeof($ip) > 0)
		{
			$this->output->unset_template();
			$google = new Google();

			$client = new Google_Client();
			$client->setClientId($google->clientId());
			$client->setClientSecret($google->clientSecret());
			$client->setRedirectUri($google->redirectUri());
			$client->addScope("email");
			$client->addScope("profile");

			$service = new Google_Service_Oauth2($client);
			$client->authenticate($_GET['code']);

			$user = $service->userinfo->get();

			$user_data = $this->user_m->getUser(array('email' => $user->email));

			if(sizeof($user_data) == 0)
			{
				$data = array(
					'email' => $user->email,
					'name' => $user->givenName, 
				);

				$user_id = $this->user_m->addUser($data);
				$user_data = $this->user_m->getUser(array('id' => $user_id));
			}



			$this->session->set_userdata(array('user_data' => $user_data));

			redirect('/project/projectList');
		}else
		{
			redirect('/login');
		}

	}

	public function admin()
	{
		if(isset($_POST['submit-admin']))
		{
			if($_POST['username'] == "admin_app" && $_POST['pass'] == "app123")
			{
				$user_data = array(
					'id' => 0, 
					'email' => 'admin@youtubeApp.com', 
					'name' => 'admin', 
				);
				$set_user_data = [];

				array_push($set_user_data,(object) $user_data);

				$this->session->set_userdata(array('user_data' => $set_user_data));
				redirect('/admin');
			}
		}

		$this->load->css('assets/css/login/login.css');

		$this->load->view('login/admin',$this->data);
	}

	
}

?>