<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		set_auth('login');
		$this->load->helper('url');
		$this->load->model('user_m');
		$this->load->model('ip_m');

		//init pages
		$this->data['page_title'] = 'Admin';
		//$this->data['no_nav'] = true;
		$this->output->set_template('default');

		
	}

	public function index()
	{

		if(isset($_POST['delete-ip']))
		{
			$this->ip_m->deleteIp($_POST['ip_id']);
		}

		if(isset($_POST['btn-add-ip']))
		{
			$data = array(
				'name' => $_POST['ip_name'],
				'IP' => $_POST['ip_address'], 
			);

			$new_ip_id = $this->ip_m->addIp($data);
		}

		$ip = $this->ip_m->getAllIp();

		$this->data['ip'] = $ip;

		$this->load->js('assets/js/admin/admin.js');

		$this->load->view('admin/ip',$this->data);


	}

	

	
	
}

?>