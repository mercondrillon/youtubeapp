<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Import extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		set_auth('login');
		$this->load->helper('url');
		$this->load->model('channel_m');
		$this->load->model('video_m');
		$this->load->model('category_m');
		$this->load->model('project_m');

		//init pages
		$this->data['page_title'] = 'import';
		$this->data['flash_data'] = $this->session->flashdata();
		$this->output->set_template('default');
		
	}


	public function url()
	{
		$user = $this->session->userdata('user_data')[0];


		$project = $this->project_m->getProject(array('userId' => $user->id));

		$this->data['project'] = $project;
		

		$this->load->js('assets/js/import/channel.js');
		$this->load->css('assets/css/import/channel.css');
		$this->load->view('import/channel',$this->data);

	}

	public function csv()
	{

		$this->load->js('assets/js/import/csv.js');
		//$this->load->css('assets/css/import/csv.css');
		$this->load->view('import/csv',$this->data);

	}

	
	
}