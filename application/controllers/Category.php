<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		set_auth('login');
		$this->load->helper('url');
		$this->load->model('channel_m');
		$this->load->model('video_m');
		$this->load->model('category_m');

		//init pages
		$this->data['page_title'] = 'Category';
		$this->output->set_template('default');

		
	}

	public function listChannels($category)
	{
		// $channel_data = $this->channel_m->getChannel(array('categoryId' => $category));

		// foreach ($channel_data as $index => $channel) {
		// 	$videos = $this->video_m->getChannelVideo($channel->id,'viewCount','asc',20);
		// 	$video_total_views_average = 0;
		// 	if(sizeof($videos) != 0)
		// 	{
		// 		foreach ($videos as $video_index => $data) {
		// 			$video_total_views_average += $data->viewCount;
		// 		}

		// 		$video_total_views_average = $video_total_views_average / sizeof($videos);
		// 	}

		// 	$channel_data[$index]->aveViews = $video_total_views_average;
		// }

		// $this->data['channel_data'] = $channel_data;
		$this->data['category'] = $this->category_m->getCategory(array('id' => $category));

		$this->load->js('assets/js/category/list-channel.js');

		$this->load->view('category/list-channel',$this->data);
	}

	
}

?>