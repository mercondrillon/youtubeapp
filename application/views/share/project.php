<div class="row" >
	<div id="container-result" >
		<form id="share-form-data" method="POST" action="<?= base_url() ?>channel/search" >
	 		<input type="hidden" id="limit" name="limit" value="<?= $data_form['limit'] ?>" >
	 		<input type="hidden" id="offset" name="offset" value="<?= sizeof($channels) ?>" >
	 		<input type="hidden" id="projectId" name="projectId" value="<?= $data_form['projectId'] ?>" >
		</form>
		<?php foreach ($channels as $index => $channel) { ?> 
			<div class="col-md-3" >
				<div class="channel-card-container" >
					<a href="<?= base_url(); ?>dashboard/view/<?= $channel->id ?>" >
					<div class="card-min" >
						<img src="<?= json_decode($channel->thumbnails)->default->url; ?>" />
						<h3><?= strlen($channel->title) > 15 ? substr($channel->title,0,15)."..." : $channel->title; ?></h3>
						<p class="card-wrap" ><?= strlen($channel->description) > 50 ? substr($channel->description,0,50)."..." : $channel->description; ?></p>
				 	</div>
				 	<ul class="list-group">
							<li class="list-group-item"><b>Email</b> <?= strlen($channel->email) > 20 ? substr($channel->email,0,20)."..." : $channel->email; ?></li>
						<li class="list-group-item"><b>Country</b> <?= getCountryName($channel->country); ?></li>
						<li class="list-group-item"><b>Views</b> <?= $channel->viewCount ?></li>
						<li class="list-group-item"><b>Subscriber</b> <?= $channel->subscriberCount ?></li>
						<li class="list-group-item"><b>Videos</b> <?= $channel->videoCount ?></li>
						<li class="list-group-item"><b>Ave. views</b> <?= $channel->ave_view ?></li>
					</ul> 
					</a>
				</div>
			</div>
		<?php } ?>
	</div>
	<div class="col-md-12" style="margin-bottom: 30px;" >
 		<div class="loader" id="search-load-bar" style="position: relative; margin-left: 50%; left: -50px; display: none;"></div>
 	</div> 
</div>