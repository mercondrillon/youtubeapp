<div class="row">
	<div class="col-md-12" >
		<table id="tbl-note" class="display" width="100%" cellspacing="0">
         <thead>
              <tr>
    			<th>Title</th>
    			<th></th>
    			<th style="width: 65%;">Notes</th>
    			<th></th>
      			</tr>
          </thead>
          <tbody>
    			<?php foreach ($note as $index => $n) { ?>
    				
    				<tr>
    					<td><a href="<?= base_url(); ?>dashboard/view/<?= $n->channel_data->id ?>" ><?= $n->channel_data->title ?></a></td>
    					<td><img src="<?= json_decode($n->channel_data->thumbnails)->default->url ?>" /></td>
    					<td><pre id="pre-<?= $index ?>" style="height: 120px;"><?= $n->note ?></pre></td>
    					<td><button class="btn btn-success" data-toggle="modal" data-target="#note-modal-<?= $index ?>"><span class="glyphicon glyphicon-pencil"></span></button><button class="btn btn-danger delete-user-note" data-index="<?= $index ?>" ><span class="glyphicon glyphicon-trash"></span></button></td>
    				</tr>
    				<div id="note-modal-<?= $index ?>" class="modal fade" role="dialog">
					  <div class="modal-dialog">

					    <!-- Modal content-->
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal">&times;</button>
					        <h4 class="modal-title">Notes</h4>
					      </div>
					      <div class="modal-body">
					      		<div class="note-msg-container-<?= $index ?>" ></div>
					      		<input type="hidden" id="note-id-<?= $index ?>" value="<?= $n->id ?>" >
					      		<textarea style="width: 100%;height: 200px;" id="user-notes-<?= $index ?>" ><?= $n->note ?></textarea>
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					        <button type="button" class="btn btn-primary save-user-note" data-index="<?= $index ?>" >Save</button>
					      </div>
					    </div>

					  </div>
					</div>
				<?php } ?>
    		</tbody>
          <tfoot>
              <tr>
    			<th>Title</th>
    			<th></th>
    			<th>Notes</th>
    			<th></th>
      			</tr>
          </tfoot>
         
      </table>
		
		
	</div>
</div>