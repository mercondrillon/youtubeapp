<div class="row" >
	<div class="col-md-12" >
		<table class="table table-striped">
    		<thead>
      			<tr>
			        <th>Ip Address</th>
			        <th>action</th>
      			</tr>
    		</thead>
    		<tbody>
	      		<?php foreach ($ip as $index => $ip_address) { ?>
	      			<tr>
	      				<td><?= $ip_address->name ?></td>
	      				<td><?= $ip_address->IP ?></td>
	      				<td>
	      					<form action="" method="POST" >
	      						<input type="hidden" name="ip_id" value="<?= $ip_address->id ?>" >
	      						<button class="btn btn-danger" name="delete-ip" >Delete</button>
      						</form>
  						</td>
	      			</tr>
	      		<?php } ?>
	      		<tr>
	      			<form action="" method="POST" >
	      				<td><input type="text" name="ip_name" id="ip_name" class="form-control" placeholder="name" ></td>
	      				<td><input type="text" name="ip_address" id="ip_address" class="form-control" placeholder="IP Address" ></td>
	      				<td><button class="btn btn-success" id="btn-add-ip" name="btn-add-ip"  >Add</button></td>
	      			</form>
	      		</tr>
			</tbody>
  		</table>
	</div>
</div>