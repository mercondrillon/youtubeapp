<div class="row">
	<div class="col-md-2" >
		<?php $image = json_decode($channel->thumbnails); ?>
		<img src="<?= $image->default->url ?>" />
		<form method="post" action="<?= base_url(); ?>channel/updateSingleChannel" >
			<input type="hidden" value="<?= $channel->id ?>" name="channel-id" />
			<button style="margin: 8px 0px;" class="btn btn-success" id="btn-update-channel-data" type="submit" >Update Channel Data</button>
		</form>
	</div>
	<div class="col-md-7 form-horizontal" >
		
		<div class="form-group">
			<label class="control-label col-sm-2" for="email">ID:</label>
			<div class="col-sm-10">
  				<p class="form-control-static"><?= $channel->yId ?></p>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-2" for="email">No. Videos:</label>
			<div class="col-sm-10">
  				<p class="form-control-static"><?= $channel->videoCount ?></p>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-2" for="email">Category:</label>
			<div class="col-sm-10">
  				<p class="form-control-static"><?= $channel->category ?></p>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-2" for="email">Country:</label>
			<div class="col-sm-10">
  				<p class="form-control-static"><?= $channel->country ?></p>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-2" for="email">Ave Viewer:</label>
			<div class="col-sm-10">
  				<p class="form-control-static"><?= $video_total_views_average ?></p>
			</div>
		</div>
		<?php if($insta){ ?>
			<div class="form-group">
				<label class="control-label col-sm-2" for="email">Instagram:</label>
				<div class="col-sm-10">
	  				<p class="form-control-static"><a href="https://instagram.com/<?= $insta->username; ?>" target="_blank" ><?= $insta->username; ?></a></p>
					<div class="col-md-6 insta-incon-con">
						<i class="fa fa-users" aria-hidden="true"></i>
						<span class="titlte-insta" >Followers</span><br/>
						<span class="count-insta" ><?= $insta->followers ?></span>
					</div>
					<div class="col-md-6 insta-incon-con">
						<i class="fa fa-heart" aria-hidden="true"></i>
						<span class="titlte-insta" >Likes</span><br/>
						<span class="count-insta" ><?= $insta->likes ?></span>
					</div>
				</div>
			</div>
		<?php } ?>
		<div class="form-group">

    		<label class="col-sm-2 control-label">Email:</label>
    		<div class="col-sm-4">
      			<div class="input-group">
  					<input type="text" class="form-control" value="<?= $channel->email ?>" id="channel-email-text"  >
      				<span class="input-group-btn">
        				<button class="btn btn-success" onclick="saveEmailChannel('<?= $channel->yId ?>');" type="button"><i style="line-height: 1.5;top: 0px;margin-top: -1px;" class="glyphicon glyphicon-ok" ></i style=""></button>
      				</span>
    			</div>
    		</div>
  		</div>
  		<div class="form-group">

    		<label class="col-sm-2 control-label">Secondary Email:</label>
    		<div class="col-sm-4">
      			<div class="input-group">
  					<input type="text" class="form-control" value="<?= $channel->second_email ?>" id="channel-second-email-text"  >
      				<span class="input-group-btn">
        				<button class="btn btn-success" onclick="saveEmailChannel('<?= $channel->yId ?>','second-email');" type="button"><i style="line-height: 1.5;top: 0px;margin-top: -1px;" class="glyphicon glyphicon-ok" ></i style=""></button>
      				</span>
    			</div>
    		</div>
  		</div>
  		<div class="form-group">

    		<label class="col-sm-2 control-label">Cost:</label>
    		<div class="col-sm-4">
      			<div class="input-group">
  					<input type="text" class="form-control" value="<?= $channel->cost ?>" id="channel-cost-text"  >
      				<span class="input-group-btn">
        				<button class="btn btn-success" onclick="saveCostChannel('<?= $channel->yId ?>');" type="button"><i style="line-height: 1.5;top: 0px;margin-top: -1px;" class="glyphicon glyphicon-ok" ></i style=""></button>
      				</span>
    			</div>
    		</div>
  		</div>
  		<div class="form-group">
			<label class="control-label col-sm-2" for="email">Url:</label>
			<div class="col-sm-10">
  				<p class="form-control-static"><a href="https://www.youtube.com/channel/<?= $channel->yId ?>" target="_blank" >https://www.youtube.com/channel/<?= $channel->yId ?></a></p>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-2" for="email">Description:</label>
			<div class="col-sm-10">
  				<pre class="form-control-static"><?= $channel->description ?></pre>
			</div>
		</div>
	</div>
	<div class="col-md-3" >
		<div class="dropdown pull-right">
			<button class="btn btn-primary dropdown-toggle " onclick="loadProject('<?= $channel->id ?>',this)" type="button" data-toggle="dropdown" style="height:28px;padding: 4px;margin-left: 3px;}" >
				Add to Project 
				<span class="caret"></span>
			</button>
			<ul class="dropdown-menu">
				<li class="project-spinner">
					<a href="javascript:void(0);" style="text-align: center;">
						<i class="fa fa-spinner fa-spin" aria-hidden="true" ></i>
					</a>
				</li>
			</ul>
		</div>
		<div class="pull-right" >


			<button class="btn btn-<?= $is_favorite? 'primary':'default'; ?>" onclick="favorite('<?= $channel->id ?>','<?= $user_data->id ?>',this)" ><span class="glyphicon glyphicon-thumbs-up" ></span></button>
			<button type="button" class="btn btn-default" data-toggle="modal" data-target="#note-modal"><span class="glyphicon glyphicon-pencil" ></span></button>
			
		</div>
	</div>
</div>
<div class="row" >
	<div class="col-md-12" >
		<h3>Top 10 Videos</h3>
		<table  class="display" width="100%" cellspacing="0" id="top-10-video-table" >
			    <thead>
		      		<tr>
		      			
				        <th>Date</th>
		                <th>Title</th>
		                <th>Views</th>
		                
			      	</tr>
			    </thead>
			    <tfoot>
		      		<tr>
		      			
				        <th>Date</th>
		                <th>Title</th>
		                <th>Views</th>
		                
			      	</tr>
			    </tfoot>
			    <tbody>
		      		<?php foreach (array_reverse($top10) as $index => $video) { ?>
		        		<tr>
		        			
		        			<td><?= date('m/d/Y', strtotime($video->date)) ?></td>
		        			<td><?= $video->title ?></td>
		        			<td><?= $video->viewCount ?></td>
		        		</tr>
		        	<?php } ?>
			    </tbody>
		  </table>
		  <h3>Other Videos</h3>
		  <table  class="display" width="100%" cellspacing="0" id="top-20-video-table" >
			    <thead>
		      		<tr>
		      			
				        <th>Date</th>
		                <th>Title</th>
		                <th>Views</th>
		                
			      	</tr>
			    </thead>
			    <tfoot>
		      		<tr>
		      			
				        <th>Date</th>
		                <th>Title</th>
		                <th>Views</th>
		                
			      	</tr>
			    </tfoot>
			    <tbody>
		      		<?php foreach (array_reverse($top20) as $index => $video) { ?>
		        		<tr>
		        			
		        			<td><?= date('m/d/Y', strtotime($video->date)) ?></td>
		        			<td><?= $video->title ?></td>
		        			<td><?= $video->viewCount ?></td>
		        		</tr>
		        	<?php } ?>
			    </tbody>
		  </table>
	</div>
</div>

<div id="note-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Notes</h4>
      </div>
      <div class="modal-body">
      		<div class="note-msg-container" ></div>
      		<input type="hidden" id="note-id" value="<?= sizeof($note) != 0? $note[0]->id:0; ?>" >
      		<input type="hidden" id="channel-note-id" value="<?= $channel->id ?>" >
       		<textarea style="width: 100%;height: 200px;" id="user-notes" ><?= sizeof($note) != 0? $note[0]->note:''; ?></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save-user-note" >Save</button>
      </div>
    </div>

  </div>
</div>