<div class="row">
	
	<?php if(!$search_status || sizeof($videos) == 0){ ?>
		<input type="hidden" id="search-status" value="" >
		<?php if($search_status){ ?>
			<div class="col-md-12" >
				<div class="alert alert-danger">
	  				<center><strong>There is no result found!</strong></center>
				</div>
			</div>
		<?php } ?>
		<div class="col-md-5" >
			<div class="search-form-conta" >
			<div id="form-search-error-log" ></div>
		 	<form method="POST" action="" id="form-search-video"  >
		 		<input type="hidden" name="search-video" value="search-video" >
		 		<input type="hidden" name="search-limit" value="12" >
		 		<input type="hidden" name="search-offset" value="0" >
	  			<div class="form-group">
	    			<label for="channel-name">Name:</label>
					<input type="text" class="form-control" id="video-name" name="video-name" />
	  			</div>	
	  		
	  			<div class="form-group">
	    			<label for="video-view" style="width: 100%;">Total view:</label>
					<input type="number" class="form-control" style="width: 20%;float: left;margin-right: 10px;" id="video-view-max" name="video-view-max" placeholder="Max" >
					<input type="number" class="form-control" style="width: 20%;" id="video-view-min" name="video-view-min" placeholder="Min" >
	  			</div>

	  			<div class="form-group">
	    			<label for="video-country">Sory By:</label>
	    			<select class="form-control" id="video-sort-type" name="video-sort-type" style="width: 40%;float: right;margin-top: 25px;" >
	    				<option value="asc" >Ascending</option>
	    				<option value="desc" >Descending</option>
	    				
	    			</select>
					<select class="form-control" id="video-sort" name="video-sort" style="width: 59%;" >
						<option value="default" >-- Sort By --</option>
						<option value="viewCount" >Views</option>
						
					</select>
	  			</div>
			  	
			  	<button class="btn btn-primary custom-btn-primary" >Search</button>
			</form> 
		</div>
	<?php }else{ ?>
		<input type="hidden" id="search-status" value="1" >
		<div id="container-result" >
		<form id="search-form-data" method="POST" action="<?= base_url() ?>channel/searchVideo" >
			<div class="form-group">
    			<label for="channel-country">Sory By:</label>
    			
    			<select class="form-control" id="video-sort-type" name="video-sort-type" style="width: 20%;float: right;margin-top: 25px;right: 44%;position: relative;" >
    				<option value="asc" <?= $data_form['video-sort-type']=='asc'? 'selected':'' ?> >Ascending</option>
    				<option value="desc" <?= $data_form['video-sort-type']=='desc'? 'selected':'' ?> >Descending</option>
    				
    			</select>
				<select class="form-control" id="video-sort" name="video-sort" style="width: 30%;/*! float: left; */" >
					<option value="default" <?= $data_form['video-sort']=='default'? 'selected':'' ?> >-- Sort By --</option>
					<option value="viewCount" <?= $data_form['video-sort']=='viewCount'? 'selected':'' ?> >Views</option>
					
					
				</select>
  			</div>
  			<input type="hidden" id="search-video" name="search-video" value="search-video" >
	 		<input type="hidden" id="search-limit" name="search-limit" value="<?= $data_form['search-limit'] ?>" >
	 		<input type="hidden" id="search-offset" name="search-offset" value="<?= $videos_size ?>" >
	 		<input type="hidden" id="video-name" name="video-name" value="<?= $data_form['video-name'] ?>" >
	 		<input type="hidden" id="video-view-max" name="video-view-max" value="<?= $data_form['video-view-max'] ?>" >
	 		<input type="hidden" id="video-view-min" name="video-view-min" value="<?= $data_form['video-view-min'] ?>" >
	 		<button class="btn btn-default" style="float: right;position: absolute;top: 97px;left: 56%;" id="btn-sort-video-func"  >Go</button>
	 	
	 	</form>
		<?php foreach ($videos as $index => $video) { ?> 
			
	 		
			<div class="col-md-3" >
				<div class="channel-card-container" >
					<a href="<?= base_url(); ?>dashboard/view/<?= $video->channelId ?>" >
					<div class="card-min" >
						<!-- <img src="<?= json_decode($channel->thumbnails)->default->url; ?>" /> -->
						<p class="card-wrap" ><?= $video->title ?></p>
				 	</div>
				 	<ul class="list-group">
  						<li class="list-group-item"><b>Views</b> <?= $video->viewCount ?></li>
					</ul> 
					</a>
				</div>
			</div>
		<?php } ?>
		</div>
		<div class="col-md-12" style="margin-bottom: 30px;" >
	 		<div class="loader" id="search-load-bar" style="position: relative; margin-left: 50%; left: -50px; display: none;"></div>
	 	</div> 
	<?php } ?>
</div>
