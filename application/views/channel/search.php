<!-- <div class="row">
	<div class="col-md-12" >
		<div class="container-4">
			<form action="" method="post" name="hyv-yt-search" id="hyv-yt-search">
			 <input type="search" name="hyv-search" id="hyv-search" placeholder="Search..." class="ui-autocomplete-input" autocomplete="off">
			 <input id="search-id" type="hidden" name="search-id" />
			 <button class="icon" id="hyv-searchBtn" type="button" disabled><span class="glyphicon glyphicon-search"></span></button>
			</form>
		</div>
	</div>
</div>
<div class="row" >
	<div class="col-md-12" >
		<table class="table table-striped" style="display:none;" id="result-table" >
		    <thead>
		      <tr>
		        <th>Titlte</th>
		        <th></th>
		        <th>Subscribers</th>
		        <th>Views</th>
		        <th>Video</th>
		        <th></th>
		        <th>Average Views</th>
		      </tr>
		    </thead>
		    <tbody id="result-table-body" >
		    </tbody>
		  </table>
	</div>
</div> -->
<div class="row">
	
	<?php if(!$search_status || sizeof($channels) == 0){ ?>
		<input type="hidden" id="search-status" value="" >
		<?php if($search_status){ ?>
			<div class="col-md-12" >
				<div class="alert alert-danger">
	  				<center><strong>There is no result found!</strong></center>
				</div>
			</div>
		<?php } ?>
		<div class="col-md-5" >
			<div class="search-form-conta" >
			<div id="form-search-error-log" ></div>
		 	<form method="POST" action="" id="form-search-channel"  >
		 		<input type="hidden" name="search-channel" value="search-channel" >
		 		<input type="hidden" name="search-limit" value="12" >
		 		<input type="hidden" name="search-offset" value="0" >
	  			<div class="form-group">
	    			<label for="channel-name">Name:</label>
					<input type="text" class="form-control" id="channel-name" name="channel-name" />
	  			</div>	
	  			<div class="form-group">
	    			<label for="channel-category">Category:</label>
					<select class="form-control" id="channel-category" name="channel-category" >
						<option value="default" >-- Select category --</option>
						<?php foreach ($categories as $index => $cat) { ?>
							<option value="<?= $cat->id ?>" ><?= $cat->name ?></option>
						<?php } ?>
					</select>
	  			</div>
	  			<div class="form-group">
	    			<label for="channel-country">Country:</label>
					<select class="form-control" id="channel-country" name="channel-country" >
						<option value="default" >-- Select country --</option>
						<?php foreach ($countries as $index => $count) { ?>
							<option value="<?= getCountryName($count->country,'short'); ?>" ><?= $count->country ?></option>
						<?php } ?>
					</select>
	  			</div>


	  			<div class="form-group">
	    			<label for="channel-subscriber" style="width: 100%;">Subscriber:</label>
					<input type="number" class="form-control" style="width: 20%;float: left;margin-right: 10px;" id="channel-subscriber-max" name="channel-subscriber-max" placeholder="Max" >
					<input type="number" class="form-control" style="width: 20%;" id="channel-subscriber-min" name="channel-subscriber-min" placeholder="Min" >
	  			</div>

	  			<div class="form-group">
	    			<label for="channel-view" style="width: 100%;">Total ave. view:</label>
					<input type="number" class="form-control" style="width: 20%;float: left;margin-right: 10px;" id="channel-view-max" name="channel-view-max" placeholder="Max" >
					<input type="number" class="form-control" style="width: 20%;" id="channel-view-min" name="channel-view-min" placeholder="Min" >
	  			</div>

	  			<div class="form-group">
	    			<label for="channel-country">Sory By:</label>
	    			<select class="form-control" id="channel-sort-type" name="channel-sort-type" style="width: 40%;float: right;margin-top: 25px;" >
	    				<option value="asc" >Ascending</option>
	    				<option value="desc" >Descending</option>
	    				
	    			</select>
					<select class="form-control" id="channel-sort" name="channel-sort" style="width: 59%;" >
						<option value="default" >-- Sort By --</option>
						<option value="cost" >Cost</option>
						<option value="ave_view" >Average View</option>
						<option value="title" >Channel Name</option>
						
					</select>
	  			</div>
			  	
			  	<button class="btn btn-primary custom-btn-primary" >Search</button>
			</form> 
		</div>
	<?php }else{ ?>
		<input type="hidden" id="search-status" value="1" >
		<div id="container-result" >
		<form id="search-form-data" method="POST" action="<?= base_url() ?>channel/search" >
			<div class="form-group">
    			<label for="channel-country">Sory By:</label>
    			
    			<select class="form-control" id="channel-sort-type" name="channel-sort-type" style="width: 20%;float: right;margin-top: 25px;right: 44%;position: relative;" >
    				<option value="asc" <?= $data_form['channel-sort-type']=='asc'? 'selected':'' ?> >Ascending</option>
    				<option value="desc" <?= $data_form['channel-sort-type']=='desc'? 'selected':'' ?> >Descending</option>
    				
    			</select>
				<select class="form-control" id="channel-sort" name="channel-sort" style="width: 30%;/*! float: left; */" >
					<option value="cost" <?= $data_form['channel-sort']=='default'? 'selected':'' ?> >-- Sort By --</option>
					<option value="cost" <?= $data_form['channel-sort']=='cost'? 'selected':'' ?> >Cost</option>
					<option value="ave_view" <?= $data_form['channel-sort']=='ave_view'? 'selected':'' ?> >Average View</option>
					<option value="title" <?= $data_form['channel-sort']=='title'? 'selected':'' ?> >Channel Name</option>
					
				</select>
  			</div>
  			<input type="hidden" name="search-channel" value="search-channel" >
	 		<input type="hidden" id="search-limit" name="search-limit" value="<?= $data_form['search-limit'] ?>" >
	 		<input type="hidden" id="search-offset" name="search-offset" value="<?= $channels_size ?>" >
	 		<input type="hidden" id="channel-name" name="channel-name" value="<?= $data_form['channel-name'] ?>" >
	 		<input type="hidden" id="channel-category" name="channel-category" value="<?= $data_form['channel-category'] ?>" >
	 		<input type="hidden" id="channel-country" name="channel-country" value="<?= $data_form['channel-country'] ?>" >
	 		<input type="hidden" id="channel-subscriber-max" name="channel-subscriber-max" value="<?= $data_form['channel-subscriber-max'] ?>" >
	 		<input type="hidden" id="channel-subscriber-min" name="channel-subscriber-min" value="<?= $data_form['channel-subscriber-min'] ?>" >
	 		<input type="hidden" id="channel-view-max" name="channel-view-max" value="<?= $data_form['channel-view-max'] ?>" >
	 		<input type="hidden" id="channel-view-min" name="channel-view-min" value="<?= $data_form['channel-view-min'] ?>" >
	 		<button class="btn btn-default" style="float: right;position: absolute;top: 97px;left: 56%;" id="btn-sort-search-func"  >Go</button>
	 		<!-- <input type="hidden" id="channel-sort-type" name="channel-sort-type" value="<?= $data_form['channel-sort-type'] ?>" >
	 		<input type="hidden" id="channel-sort" name="channel-sort" value="<?= $data_form['channel-sort'] ?>" > -->
	 	</form>
		<?php foreach ($channels as $index => $channel) { ?> 

			<div class="col-md-3" >
				<div class="channel-card-container" >
					<a href="<?= base_url(); ?>dashboard/view/<?= $channel->id ?>" >
					<div class="card-min" >
						<img src="<?= json_decode($channel->thumbnails)->default->url; ?>" />
						<h3><?= strlen($channel->title) > 15 ? substr($channel->title,0,15)."..." : $channel->title; ?></h3>
						<p class="card-wrap" ><?= strlen($channel->description) > 50 ? substr($channel->description,0,50)."..." : $channel->description; ?></p>
				 	</div>
				 	<ul class="list-group">
  						<li class="list-group-item"><b>Email</b> <?= strlen($channel->email) > 20 ? substr($channel->email,0,20)."..." : $channel->email; ?></li>
						<li class="list-group-item"><b>Country</b> <?= getCountryName($channel->country); ?></li>
						<li class="list-group-item"><b>Views</b> <?= $channel->viewCount ?></li>
						<li class="list-group-item"><b>Subscriber</b> <?= $channel->subscriberCount ?></li>
						<li class="list-group-item"><b>Videos</b> <?= $channel->videoCount ?></li>
						<li class="list-group-item"><b>Ave. views</b> <?= $channel->ave_view ?></li>
						<li class="list-group-item">
							<b>Instagram</b> 
							<?php if($channel->insta){ ?>
								<?php 
									echo $channel->insta->username;
								?>
							<?php }else{ ?>
								None
							<?php } ?>
						</li>
					</ul> 
					</a>
				</div>
			</div>
		<?php } ?>
		</div>
		<div class="col-md-12" style="margin-bottom: 30px;" >
	 		<div class="loader" id="search-load-bar" style="position: relative; margin-left: 50%; left: -50px; display: none;"></div>
	 	</div> 
	<?php } ?>
</div>
