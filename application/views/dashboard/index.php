<div class="row" >
	<div class="col-md-2" >
		<b>Category</b>
	 	<div class="list-group">
	 		<?php foreach ($categories as $cat_index => $cat) { ?>
				<a href="<?= base_url() ?>category/listChannels/<?= $cat->id ?>" target="_blank" class="list-group-item"><?= $cat->name ?></a>
			<?php } ?>
		</div> 
	</div>
	<div class="col-md-10" >
	 	<!-- <div class="form-group">
	  		<label for="sel1">Filter by Category:</label>
	  		<select class="form-control" id="filter-category">
	  			<option value="default" selected ></option>
	    		<?php foreach ($categories as $cat_index => $cat) { ?>
	    			<option value="<?= $cat->id ?>" ><?= $cat->name ?></option>
	    		<?php } ?>
		  	</select>
		</div>  -->
		<table id="tbl-channels-list" class="display" width="100%" cellspacing="0">
	        <thead>
	            <tr>
	                <th>Title</th>
	                <th>Email</th>
	                <th></th>
	                <th>Subscriber</th>
	                <th>Ave. Views</th>
	                
	            </tr>
	        </thead>
	        <tfoot>
	            <tr>
	               <th>Title</th>
	               <th>Email</th>
	               <th></th>
	               <th>Subscriber</th>
	                <th>Ave. Views</th>
	            </tr>
	        </tfoot>
	       <!--  <tbody>
	       		
	        	<?php foreach ($channel_data as $index => $channel) { ?>
	        		
		        		<tr>
		        			<td><a href="<?= base_url(); ?>dashboard/view/<?= $channel->id; ?>" target="_blank" ><?= $channel->title ?></a></td>
		        			<td><?= $channel->subscriberCount ?></td>
		        			<td><?= $channel->aveViews ?></td>
		        		</tr>
	        	
	        	<?php } ?>
	        </tbody> -->
	    </table>
		

 	</div>
</div>