<div class="row">
	<div class="col-md-4 col-md-offset-4" >
		<div class="login-container" >
		 	<form method="POST" action="" >
  				<div class="form-group">
    				<label for="email">Username:</label>
    				<input type="text" class="form-control" id="username" name="username" >
  				</div>
  				<div class="form-group">
    				<label for="pwd">Password:</label>
    				<input type="password" class="form-control" id="pass" name="pass" >
  				</div>
  				<button type="submit" class="btn btn-default" name="submit-admin" >Submit</button>
			</form>
			<!-- <center><p>OR</p></center> -->
			<!-- <a href="<?= $authUrl ?>" class="btn btn-danger" >Login With Google</a>  -->
		</div>
	</div>
</div>