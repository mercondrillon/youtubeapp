<div class="row">
    <div class="col-sm-6 col-sm-offset-3">
      <?php if(sizeof($flash_data) > 0){ ?>
      <?php if(!$flash_data['status']){ ?>
        <div class="alert alert-danger">
          <strong>Error!</strong> <?= $flash_data['msg'] ?>.
        </div>
      <?php } ?>
    <?php } ?>
      <form method="POST" id="asin-search-in" action="<?= base_url(); ?>product/searchPrudoct" >
          <div id="imaginary_container"> 
              <div class="input-group stylish-input-group">
                  <input type="text" class="form-control"  placeholder="Search item ASIN" name="ASIN" id="ASIN"   >
                  <span class="input-group-addon">
                      <button type="submit">
                          <span class="glyphicon glyphicon-search"></span>
                      </button>  
                  </span>
              </div>
          </div>
        </form>
    </div>
    <div class="col-md-3" >
      <a href="<?= base_url() ?>product/addBulk" class="btn btn-default" >Add bulk of ASIN</a>
    </div>
</div>
<?php if(sizeof($flash_data) > 0){ ?>
  <?php if($flash_data['status']){ ?>
    <?php $item = $flash_data['new_item']; ?>
    <div class="row">
      <div class="col-sm-6 col-sm-offset-3">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <img src="<?= $item['img'] ?>" alt="" class="img-rounded img-responsive" />
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <h4><?= $item['asin']; ?></h4>
                        <small><cite title=""><?= $item['title'] ?></cite></small>
                        <p>
                            <?= $item['sellerNickname'] ?>
                            <br />
                            <?php if($item['price'] != 'none'){ ?>
                              <?= json_decode($item['price'])->FormattedPrice ?>
                            <?php }else{ ?>
                              <?= $item['price'] ?>
                            <?php } ?>
                            <br />
                            Stock: <?= $item['quantity'] ?>
                            <br/>
                        </p>
                        <!-- Split button -->
                        <a href="<?= base_url() ?>product/track/<?= $item['id'] ?>" class="btn btn-primary" >View Track table</a>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
  <?php } ?>
<?php } ?>
<div class="row" >
  <div class="col-md-12" >
  <?php if($product){  ?>
    <table class="table table-hover" id="table-product" >
      <thead>
        <tr>

          <th style="text-align: center;" >Image</th>
          <th style="text-align: center;" >ASIN</th>
          <th style="width: 500px;" >Title</th>
          <th style="text-align: center;" >Today Stock</th>
          <th style="text-align: center;" ></th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($product as $index => $p) { ?>
          <tr>
            <td  style="text-align: center;" ><img src="<?= $p->img ?>" ></td>
            <td  style="text-align: center;" ><?= $p->asin ?></td>
            <td><?= $p->title ?></td>
            <td  style="text-align: center;" ><?= todayStock($p->id) ?></td>
            <td  style="text-align: center;" >
              <a href="<?= base_url() ?>product/track/<?= $p->id ?>" class="btn btn-primary" ><i class="fa fa-bar-chart" aria-hidden="true"></i>
</a>
              <a href="<?= $p->detailUrl ?>" target="_blank" class="btn btn-default" ><i class="fa fa-external-link-square" aria-hidden="true"></i>
</a>
              <button class="btn btn-danger btn-delete-prod" data-prod-id="<?= $p->id ?>" ><i class="fa fa-trash" aria-hidden="true"></i>
</button>
            </td>
          </tr>
        <?php } ?>    
      </tbody>
    </table>      
  <?php }else { ?>
    <div class="alert alert-warning">
      No product avalable.
    </div>
  <?php } ?>
    
  </div>
</div>