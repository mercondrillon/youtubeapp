<div class="row" >
	<div class="col-md-12" >
		<?php if(sizeof($flash_data) > 0){ ?>
			<div class="alert alert-success">
			  <strong>Success!</strong> <?= $flash_data['msg'] ?>.
			</div>
		<?php } ?>
		<form method="POST" action="<?= base_url() ?>product/saveBulkProduct" id="form-add-bulk-asin" >
		<input type="hidden" id="asin" name="asin" />
			<div class="form-group">
				<label for="comment">Put bulk of product ASIN:</label>
				<textarea class="form-control" rows="5" id="asin-bulk" name="asin-bulk" ></textarea>
			</div> 
			<button class="btn btn-primary" >Add</button>
		</form>
	</div>
</div>