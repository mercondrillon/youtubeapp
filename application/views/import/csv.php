<div class="row" >

	<div class="col-md-12" >
		<?php if(sizeof($flash_data) > 0){ ?>
			<div class="alert alert-success">
			  <strong>Success!</strong> <?= $flash_data['msg'] ?>.
			</div>
		<?php } ?>
	 	<form class="form-inline" action="<?= base_url(); ?>channel/csvFile" method="POST" enctype='multipart/form-data' id="import-csv-form"  >
			<div class="form-group">
    			<label for="email">Import CSV file </label>
    			<input type="file" class="form-control" name="csv-file" id="csv-file" >
  			</div>
		</form> 
	</div>
	<!-- <div class="col-md-9" >
		<i class="fa fa-circle-o-notch fa-2x fa-spin" aria-hidden="true" id="loader-icon-import" style="display: none;" ></i>
		<iframe src="" id="load-import-csv" name="load-import-csv"  style="width: 100%;height: 100%;border: none;"></iframe>
	</div> -->
</div>