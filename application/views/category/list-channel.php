<div class="row">
	<div class="col-md-12" >
		<h3><?= $category[0]->name ?></h3>
		<input type="hidden" id="category-id" value="<?= $category[0]->id ?>" />
		<table id="category-channel-list" class="display" width="100%" cellspacing="0">
	       <thead>
	            <tr>
	                <th>Title</th>
	                <th>Email</th>
	                <th></th>
	                <th>Subscriber</th>
	                <th>Ave. Views</th>
	                
	            </tr>
	        </thead>
	        <tfoot>
	            <tr>
	               <th>Title</th>
	               <th>Email</th>
	               <th></th>
	               <th>Subscriber</th>
	                <th>Ave. Views</th>
	            </tr>
	        </tfoot>
	       
	    </table>
	</div>
</div>