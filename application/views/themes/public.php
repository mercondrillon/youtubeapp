<html lang="en">
	<head>
		<title>Youtube App | <?php echo $page_title; ?></title>
		<meta name="resource-type" content="document" />
		<meta name="robots" content="all, index, follow"/>
		<meta name="googlebot" content="all, index, follow" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" type="text/css" />
		<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/vendor/datatable/css/jquery.dataTables.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap3/bootstrap-switch.min.css">
		<link rel="stylesheet" href="<?= base_url(); ?>assets/vendor/css/notie.min.css">
		<link rel="stylesheet" href="<?= base_url(); ?>assets/css/global/main.css">
		<?php
			foreach($css as $file){
			 	echo "\n\t\t";
				?><link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" /><?php
			} echo "\n\t";

		?>

	</head>

  	<body>
  		<?php
  			$nav = true;
  			if(isset($no_nav))
  			{
  				if($no_nav)
  					$nav =false;
  			}
  		
  		?>

  		<div id="global-loader" style="display: none; position: fixed; width: 100%; height: 100%; z-index: 999999; background-color: rgba(16,16,16,0.7);" ><div style="width: 30px;height: 30px;position: relative;margin-left: -15px;left: 50%;margin-top: -15px;top: 50%;" ><i class="fa fa-circle-o-notch fa-spin fa-3x" style="color: #ddd;" aria-hidden="true"></i></div></div>

  		<?php if($nav){ ?>
  			
		 	<nav class="navbar navbar-inverse">
		  		<div class="container-fluid">
			    	<div class="navbar-header">
			      		<a class="navbar-brand" href="<?= base_url(); ?>dashboard">YoutubeApp</a>
			    	</div>
				    <ul class="nav navbar-nav navbar-right">
				      <li class=""><a href="<?= base_url(); ?>login">Login</a></li>
				    </ul>
			  	</div>
			</nav>
			
		<?php } ?> 
    	<div class="container" >
	  		<?php echo $output;?>
	      	<footer>
	      		<script type="text/javascript" src='<?= base_url(); ?>assets/vendor/js/jquery.min.js'></script>
				<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js" type="text/javascript"></script>
				<script type="text/javascript" src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>
				<script type="text/javascript" src='<?= base_url(); ?>assets/vendor/datatable/js/jquery.dataTables.min.js'></script>
				<script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.min.js'></script>
				<script type="text/javascript" src='<?= base_url(); ?>assets/vendor/js/notie.min.js'></script>
				<script type="text/javascript"> var base_url = "<?php echo base_url(); ?>"; </script>
				<script type="text/javascript" src='<?= base_url(); ?>assets/js/global/main.js'></script>
				
		      	<?php
		      		foreach($js as $file){
							echo "\n\t\t";
							?><script src="<?php echo $file; ?>"></script><?php
					} echo "\n\t";
		      	?>
	      	</footer>
	    </div>
	   
	</body>
</html>
