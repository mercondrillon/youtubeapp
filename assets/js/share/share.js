$(document).ready(function(){
	var ajax_scroll = true;
     window.onscroll = function(ev) {
         if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
               
                var limit = parseInt($("#limit").val());
                var offset = parseInt($("#offset").val());
                if(offset >= limit)
                {

                     if(ajax_scroll)
                     {
                          ajax_scroll = false;
                          $("#search-load-bar").show();

                          var search_data = new FormData(document.getElementById("share-form-data"));

                          $.ajax({
                               url:base_url+'share/loadChannelShare',
                               type:'post',
                               data: search_data,
                               processData: false,
                               contentType: false,
                               dataType:'json',
                               success:function(data)
                               {
                                  
                                    ajax_scroll = true;
                                    $("#search-load-bar").hide();
                                    $("#offset").val(data.size);
                                    
                                    data.channel.forEach(function(index){

                                         var img="";
                                         if(index.thumbnails != "")
                                         {
                                              img = JSON.parse(index.thumbnails).default.url;
                                         }
                                         
                                         var parent_div = $('<div class="col-md-3" ></div>');
                                         var card_container = $('<div class="channel-card-container" ></div>');
                                         var a = $('<a href="'+base_url+'dashboard/view/'+index.id+'" ></a>');
                                         var card_min = $('<div class="card-min" ></div>');
                                         var img = $('<img src="'+img+'" />');

                                         if(index.title.length > 15)
                                         {
                                              title = index.title.substring(0,15)+'...';
                                         }else
                                         {
                                              title = index.title;
                                         }

                                         var h3 = $('<h3>'+title+'</h3>');

                                         if(index.description.length > 20)
                                         {
                                              description = index.description.substring(0,20)+'...';
                                         }else
                                         {
                                              description = index.description;
                                         } 
                                         var p = $('<p class="card-wrap" >'+description+'</p>');
                                         var ul = $('<ul class="list-group" ></ul>');

                                         if(index.email.length > 20)
                                         {
                                              email = index.email.substring(0,20)+'...';
                                         }else
                                         {
                                              email = index.email;
                                         }

                                         var li_email = $('<li class="list-group-item"><b>Email</b> '+email+'</li>');
                                         var li_country = $('<li class="list-group-item"><b>Country</b> '+index.country+'</li>');
                                         var li_views = $('<li class="list-group-item"><b>Views</b> '+index.viewCount+'</li>');
                                         var li_subscriber = $('<li class="list-group-item"><b>Subscriber</b> '+index.subscriberCount+'</li>');
                                         var li_videos = $('<li class="list-group-item"><b>Videos</b> '+index.videoCount+'</li>');
                                         var li_ave = $('<li class="list-group-item"><b>Ave. Views</b> '+index.ave_view+'</li>');

                                         //append header card
                                         card_min.append(img);
                                         card_min.append(h3);
                                         card_min.append(p);

                                         //append info of card
                                         ul.append(li_email);
                                         ul.append(li_country);
                                         ul.append(li_views);
                                         ul.append(li_subscriber);
                                         ul.append(li_videos);
                                         ul.append(li_ave);

                                         //contain in a tag
                                         a.append(card_min);
                                         a.append(ul);

                                         //appned to card container
                                         card_container.append(a);

                                         //append on parent
                                         parent_div.append(card_container);

                                         $("#container-result").append(parent_div);



                                    });
                                    //$("#container-result").append(data.channel);
                               }
                          });
                     }
                }
               
             
         }
     };
});