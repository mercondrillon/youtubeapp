var table_channel;
var switch_all_checked_item = false;
var current_id;
$(document).ready(function(){
	$("#tbl-list").hide();
	dev_local_btn(false);
	$("#sel-list").change(function(){
		var id = $(this).val();
		current_id = id;
		if ( $.fn.DataTable.isDataTable( '#tbl-list' ) ) {
			table_channel.destroy();
		}

		$("#tbl-list").hide();
		$("#channel-info-panel").hide();
		$("#panel-action-project").hide();
		$("#tbl-list-body").empty();

		$("body").addClass("noscroll");
		$("#global-loader").show();

		if( checkLocalStorage() ){
			if( checkLocal(id) ){
				console.log("Getting Local");
				expiry_local_data(id);
				setTimeout( function(){
					getLocal(id);
					$("#global-loader").hide();
				} , 2000 ) ;
			}else{
				if(id != "default")
				{
					expiry_local_data(id);
					getProjectDataV2(id);
					if( checkLocal(id) ){
						getLocal(id);
					}else{
						loadProjectData(id);
					}
				}
			}
		}else{
			if(id != "default")
			{
				loadProjectData(id);
			}
		}

	});

	$("#btn-edit-project").click(function(){
		var text = $('#project-new-name').val();
		var projectId = $("#sel-list").val();
		if(text.replace(/\s/g,'') != "")
		{
			
			$.ajax({
				url:base_url+'project/edit',
				type:'post',
				data:{projectId:projectId,text:text},
				success:function(data){
					
					$('#edit-project-name').modal('hide');
					$("#sel-list option:selected").text(text);
					notie.alert({type:'success',text:'Project successfully saved!',time:2});
				}
			});

		}else
		{
			notie.alert({type:'error',text:'The project name is empty',time:2});
		}
		
	});

	$("#btn-delete-project").click(function(){
		notie.confirm({
			text: 'Are you sure to delete this project?',
			submitCallback:function(){
				notie.alert({type:'warning',text:'Deleting project ...',stay:true});
				var id = $("#sel-list option:selected").val();
				$("#channel-info-loader").show();
				$.ajax({
					url:base_url+'project/deleteProject',
					type:'post',
					data:{id:id},
					success:function(data){
						$("#channel-info-loader").hide();
						notie.alert({type:'success',text:'Project successfully deleted',time:2});
						$("#tbl-list").hide();
						$("#channel-info-panel").hide();
						$("#panel-action-project").hide();
						$("#tbl-list-body").empty();
						$("#sel-list option:selected").remove();
						
					}
				});
			}
		});	
	});

	$("#btn-save-project-info").click(function(){
		var tp1 = $("#channel-tp1").val();
		var tp2 = $("#channel-tp2").val();
		var tp3 = $("#channel-tp3").val();
		var url = $("#channel-offer-url").val();
		var cost_rate = $("#channel-cost-rate").val();
		var sale_profit = $("#channel-sale-profit").val();
		var projectId = $("#sel-list").val();
		notie.alert({type:'warning',text:'Saving...',stay:true});
		$("#channel-info-loader").show();
		$.ajax({
			url:base_url+'project/edit',
			type:'post',
			data:{
				tp1:tp1,
				tp2:tp2,
				tp3:tp3,
				url:url,
				cost_rate:cost_rate,
				sale_profit:sale_profit,
				option:'info',
				projectId:projectId
			},
			dataType:'json',
			success:function(data){
				$("#channel-info-loader").hide();
				$("#estimated-sales").html(data.estemated_sales+"");
				$("#estimated-gross").html(data.estemated_gross+"");
				$("#estimated-net").html(data.estemated_net+"");
				notie.alert({type:'success',text:'Project successfully saved.',time:3});
			}

		});

	});

	$("#btn-duplicate-project").click(function(){

		var projectId = $("#sel-list").val();
		notie.confirm({
			text:'Are you sure to duplicate this project?',
			submitCallback:function(){
				notie.alert({type:'warning',text:'Duplicating...',stay:true});
				$.ajax({
					url:base_url+'project/duplicateProject',
					type:'post',
					data:{projectId:projectId},
					dataType:'json',
					success:function(data)
					{

						notie.alert({type:'success',text:'successfully duplicate project',time:3});
						var option = $("<option value='"+data.id+"' >"+data.name+"</option>");
						 $("#sel-list").append(option);
					}

				});
			}
		});
	});

	$("#btn-refresh-project").click(function(){
		notie.alert({type:'warning',text:'Updating channel data in this project...',stay:true});
		$("body").addClass("noscroll");
		$("#global-loader").show();

		var projectId = $("#sel-list").val();

		$.ajax({
			type:'post',
			url:base_url+'channel/refreshProjectChannel',
			data:{projectId:projectId},
			dataType:'json',
			success:function(data){
				notie.alert({type:'success',text:'successfully update project channel',time:3});

				if ( $.fn.DataTable.isDataTable( '#tbl-list' ) ) {
		  			table_channel.destroy();
				}
				$("#tbl-list-body").empty();

				loadProjectData(projectId);
			}
		});
	});

	$("#btn-export-project").click(function(){
		notie.confirm({
			text:'Are you sure to export this project?',
			submitCallback:function(){
				$("#global-loader").show();
				$("body").addClass('noscroll');
				var projectId = $("#sel-list").val();
				$.ajax({
					url:base_url+'project/exportProject',
					type:'post',
					data:{projectId:projectId},
					success:function(data)
					{
						$("#global-loader").hide();
						$("body").removeClass('noscroll');
						$("#download-csv-project").attr('src',base_url+'project/downloadCsv/'+data);
					}

				});
			}
		});
		

	});

	$(".parent-item-check").click(function(){
		var parent = $(this).prop( "checked" );
		$(".check-item").each(function(){
			$(this).prop('checked', parent);			
		});
	});

	var load_checker = 0;

	$("#load-bulk-channel").on('load',function(){
		if(load_checker == 0){
			load_checker++;
		}else{
			load_checker=0;
			return false;
		}
		console.log("load-bulk-channel");

		var total_ave_view = 0;
		var total_cost = 0;
		var channel_count = table_channel.rows().count();
		var cost_rate = $("#channel-cost-rate").val();
		var channel_sale_profit = $("#channel-sale-profit").val();

		var id = $("#sel-list").val();

		table_channel.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
		    var data = this.data();
		    var status = data[9];
		    var num_videos =  data[10];
		    var ave_views = data[11];
		    var cost = data[12];
		    var x = $(this.node());

		    //computation starts here
		    if(status == 1 || status == "1"){
		    	total_ave_view += parseInt(ave_views);
		    	total_cost += parseInt(cost);
		    }
		} );
		
		var estimated_sales = parseInt(total_ave_view) * parseInt(cost_rate);
		var estimated_gross = parseInt(estimated_sales) * parseInt(channel_sale_profit);
		var estimated_net = parseInt(estimated_gross) - parseInt(total_cost);

		//update computation display

		$("#channel-combined-average-view").html(total_ave_view+"");
		$("#channel-cost").html(total_cost+"");
		$("#estimated-sales").html(estimated_sales+"");
		$("#estimated-gross").html(estimated_gross+"");
		$("#estimated-net").html(estimated_net+"");
		if( checkLocalStorage() ){
			getProjectDataV2(current_id);
		}
		setTimeout( function(){
			$("body").removeClass("noscroll");
			$("#global-loader").hide();
			$("#load-bulk-channel").attr('src','about:blank');
			local_compute(current_id);
		} , 5000 );
	});

	$("#btn-share-project").click(function(){
		notie.alert({type:'warning',text:'Creating share link for this project',stay:true});
		
		$.ajax({
			url:base_url+"project/getShareLink",
			type:'post',
			data:{projectId:$("#sel-list").val()},
			success:function(link){
				notie.input({text:'Shareable link',submitText:'Copy to Clipboard',cancelText:'Close',value:link,submitCallback:function(value){
					// var input_hidden = $('<input type="hidden" value="'+value+'" id="input-hidden-link" >');
					// $("body").append(input_hidden);
					// $(".notie-input-field").focus();
					// succeed = document.execCommand("copy");
					//$("#input-hidden-link").remove();
				}});
				$(".notie-input-field").attr('id','input-share-link');
				$(".notie-background-success").attr('data-clipboard-target','#input-share-link');
				var clipboard = new Clipboard('.notie-background-success');
				//Clipboard('.notie-background-success');
				
			}
		});

	});



});

// Check if localstorage is available
function checkLocalStorage(){
	if (Modernizr.localstorage) {
	  	// localStorage is supported
		return true;
	} else {
		return false;
	  	// localStorage not supported
	}
}

function deleteItem(itemId,btn)
{
	var parent = $(btn).parent().parent();
	notie.confirm({
		text:'Are to delte this items',
		submitCallback:function(){
			$.ajax({
				url:base_url+'project/deleteItem',
				type:'post',
				data:{itemId:itemId},
				dataType:'json',
				success:function(data){

					$("#channel-combined-average-view").html(data.combine_ave);
					$("#channel-cost").html(data.combine_cost);
					$("#estimated-sales").html(data.estemated_sales+"");
					$("#estimated-gross").html(data.estemated_gross+"");
					$("#estimated-net").html(data.estemated_net+"");
					parent.remove();
				}
			});
		}
	});
	
}


function loadProjectData(id)
{
	$(".parent-item-check").prop('checked',false);
	$.ajax({
		url:base_url+'project/getProjectItems',
		type:"post",
		data:{id:id},
		dataType:'json',
		success:function(data){
			
			$("body").removeClass("noscroll");
			$("#global-loader").hide();

			$("#tbl-list").show();
			$("#channel-info-panel").show();
			$("#panel-action-project").show();

			$("#project-new-name").val($("#sel-list option:selected").text());

			$("#channel-tp1").val(data.project.tp1);
			$("#channel-tp2").val(data.project.tp2);
			$("#channel-tp3").val(data.project.tp3);
			$("#channel-offer-url").val(data.project.offer);
			$("#channel-sale-profit").val(data.project.sale_profit);
			$("#channel-cost-rate").val(data.project.cost_rate);
			$("#channel-combined-average-view").html(data.project.combine_ave);
			$("#channel-cost").html(data.project.combine_cost);
			$("#estimated-sales").html(data.project.estemated_sales+"");
			$("#estimated-gross").html(data.project.estemated_gross+"");
			$("#estimated-net").html(data.project.estemated_net+"");
			
			
			data.items.forEach(function(item){
				var tr = $("<tr id="+item.id+"></tr>");
				tr.append('<td><label class="checkbox-inline"><input type="checkbox" class="check-item" data-check-id="'+item.id+'" style="margin-left:-12px;" value=""></label></td>');
				tr.append('<td><img src="'+item.img+'" width="50" /></td>');
				tr.append('<td><a href="'+base_url+'dashboard/view/'+item.channelId+'" >'+item.title+'</a></td>');
				if(item.email != "")
					var email = item.email;
				else
					var email = "none";


				if(item.is_active == "1"){
					var ave_view_num = "";
					var is_active = "checked";
					var active_number = 1;
				}
				else{
					var ave_view_num = '';
					var is_active = "";
					var active_number = 2;
				}
			

				
				tr.append('<td>'+email+'</td>');				
				tr.append('<td>'+item.video+'</td>');
				tr.append('<td><span id="ave-view-num-container-'+item.id+'" style="'+ave_view_num+'" >'+item.ave_view+'</span></td>');
				tr.append('<td>'+item.cost+'</td>');
				tr.append('<td><button class="btn btn-danger" onclick="deleteItem(\''+item.id+'\',this)" ><span class="glyphicon glyphicon-trash" ></span></button></td>');
				tr.append('<td><div class="col-md-4"><input id="sw'+item.id+'" type="checkbox" name="my-checkbox" class="button-checkbox" data-item-id="'+item.id+'" data-on-text="On" data-off-text="Off" '+is_active+' ></div></td>');
				tr.append('<td id="state'+item.id+'" class="hidden">'+active_number+'</td>');
				tr.append('<td>'+item.video+'</td>');
				tr.append('<td>'+item.ave_view+'</td>');
				tr.append('<td>'+item.cost+'</td>');
				$("#tbl-list-body").append(tr);
				
			});

			$('.button-checkbox').bootstrapSwitch();
			$('.button-checkbox').on('switchChange.bootstrapSwitch', function (event, state) { 
				var state = $(this);

				if( $("#state"+state.data("item-id") ).html() == "1" ){
					$("#state"+state.data("item-id") ).html("2");
				}else{
					$("#state"+state.data("item-id") ).html("1"); 
				}
			});
			$('.button-checkbox').on('switchChange.bootstrapSwitch', function (event, state) {
				if(!switch_all_checked_item)
				{
					var is_active = $(this).prop("checked");
					var item_id = $(this).data('item-id');
					$("#channel-info-loader").show();
					// $("#global-loader").show();
					// $("body").addClass('noscroll');

					$(".button-checkbox").bootstrapSwitch('disabled',true);
					$.ajax({
						url:base_url+'project/projectUpdateItem',
						type:'post',
						data:{is_active:is_active,item_id:item_id},
						dataType:'json',
						success:function(data){
							$(".button-checkbox").bootstrapSwitch('disabled',false);
							// $("#global-loader").hide();
							// $("body").removeClass('noscroll');
							// if(!$("#ave-view-num-container-"+item_id).is(':visible'))
							// 	$("#ave-view-num-container-"+item_id).show();
							// else
							// 	$("#ave-view-num-container-"+item_id).hide();

							$("#channel-info-loader").hide();
							$("#channel-combined-average-view").html(data.combine_ave);
							$("#channel-cost").html(data.combine_cost);
							$("#estimated-sales").html(data.estemated_sales+"");
							$("#estimated-gross").html(data.estemated_gross+"");
							$("#estimated-net").html(data.estemated_net+"");
						}
					});
				}
				
			}); 
			

			table_channel = $("#tbl-list").DataTable({
				"lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],
				'columnDefs': [
				    { 'orderData':[9], 'targets': [8] },
				    {"targets":7,"orderable":false}
				],
				"initComplete": function( settings, json ) {
    				var select = $('<select class="form-control" id="sel-bulk-action" name="method" style="display: inline-block;width: 60%;margin-right: 5px;" ></select>');
    				select.append('<option value="default" >--</option>');
    				select.append('<option value="update" >Update Data</option>');
    				select.append('<option value="activate" >Activate</option>');
    				select.append('<option value="deactivate" >Deactivate</option>');

    				var div_col_3 = $('<div class="col-md-3" ></div>');
    				div_col_3.append(select);
    				var button_go = $('<button class="btn btn-default" >Go</button>');

    				div_col_3.append(button_go);

    				var parent_div = $('<div class="select-custom-contaner" ></div>');
    				var form = $('<form action="'+base_url+'project/UpdateBulkItems" method="post" target="load-bulk-channel">');

    				form.submit(function(){
    					
    					if($(".check-item:checked").length != 0)
						{
							$("body").addClass("noscroll");
							$("#global-loader").show();
							
	    					var method = $("#sel-bulk-action").val();
	    					switch_all_checked_item = true;
							var channel_id_arr = [];
							var count = 0;
	    					$(".check-item:checked").each(function(){

	    						if(method == "activate")
								{
									$('.button-checkbox[data-item-id="'+$(this).data('check-id')+'"]').bootstrapSwitch('state', true);
								}
								if(method == "deactivate")
								{
									$('.button-checkbox[data-item-id="'+$(this).data('check-id')+'"]').bootstrapSwitch('state', false);
								}

								channel_id_arr.push($(this).data('check-id'));
								count++;

								if(count == $(".check-item:checked").length)
								{
									switch_all_checked_item = false;
									$("#channel_ids").val(JSON.stringify(channel_id_arr));
								}

	    					});
	    					setTimeout(function(){
	    						$("#load-bulk-channel").show();
	    					},2000);
	    					
	    				}else
	    				{
	    					return false;
	    				}
    				});

    				var channel_arr_input = $('<input type="hidden" name="channel_ids" id="channel_ids" value="" >');
    				var bulk_project_id = $('<input type="hidden" name="projectId" id="projectId" value="'+$("#sel-list").val()+'" >');

    				form.append(channel_arr_input);
    				form.append(bulk_project_id);
    				form.append(div_col_3);

    				parent_div.append(form);
    				//parent_div.append(div_col_1);
    				
    				$("#tbl-list_filter").after(parent_div);
  				}
			});
			// table_channel.column(9).visible(false);
			table_channel.column(10).visible(false);
			table_channel.column(11).visible(false);
			table_channel.column(12).visible(false);
		}
	});
}

// Get/Load Project Data
function getProjectDataV2(id){
	$(".parent-item-check").prop('checked',false);
	console.log("getProjectDataV2");
	$.ajax({
		url:base_url+'project/getProjectItemsv2',
		type:"post",
		data:{id:id},
		dataType:'json',
		success:function(data){ 
			if( id == data.project.id ){
				setLocal(data);
				console.log("getProjectDataV2 - setLocal");
			}
		}
	});
}

Storage.prototype.setObject = function(key, value) {
    this.setItem(key, JSON.stringify(value));
}

Storage.prototype.getObject = function(key) {
    var value = this.getItem(key);
    return value && JSON.parse(value);
}

// storing up html5 local storage
function setLocal(data){
	console.log("Setting Local Data");
	localStorage.setObject("project"+data.project.id, data);
}

// getting html5 storage data
function getLocal(projectId){
	console.log("Getting Local Data");
	generateContent( localStorage.getObject("project"+projectId) );
}

//check if html storage name exist
function checkLocal(projectId){
	if( localStorage.getItem("project"+projectId) === null){
		return false;
	}else{
		return true;
	}
}

// clearing local storage
function clearLocal(){
	localStorage.clear();
}

//compute things locally
function local_compute(projectId){
	if( localStorage.getObject("project"+projectId) === null ){
		return false;
	}else{
		data = localStorage.getObject("project"+projectId);

		var total_ave_view = 0;
		var total_cost = 0;
		var cost_rate = data.project.cost_rate;
		var channel_sale_profit = data.project.sale_profit;

		data.items.forEach(function(item){ 
			 //computation starts here
		    if(item.is_active == 1 || item.is_active == "1"){
		    	total_ave_view += parseInt(item.ave_view);
		    	total_cost += parseInt(item.cost);
		    }
		});

		var estimated_sales = parseInt(total_ave_view) * parseInt(cost_rate);
		var estimated_gross = parseInt(estimated_sales) * parseInt(channel_sale_profit);
		var estimated_net = parseInt(estimated_gross) - parseInt(total_cost);


		console.log($("#channel-combined-average-view").html());
		$("#channel-combined-average-view").html(total_ave_view+"");
		$("#channel-cost").html(total_cost+"");
		$("#estimated-sales").html(estimated_sales+"");
		$("#estimated-gross").html(estimated_gross+"");
		$("#estimated-net").html(estimated_net+"");
	}
}

function clearLocalById(id){
	localStorage.removeItem("project"+id);
}

function dev_local_btn(flag){
	if(flag){
		$(".container").append("<button onclick='dev_show_local_data();'>Test</button>");
		$(".container").append("<button onclick='localStorage.clear();'>Clear</button>");
		$(".container").append("<button onclick='expiry_local_data(15);'>Expiry</button>");
	}
}

function dev_show_local_data(){
	console.log( localStorage.getItem("project15") );
	data = localStorage.getObject("project15");		
}

function expiry_local_data(projectId){
	if( localStorage.getObject("project"+projectId) === null ){
		console.log("ASD");
		return false;
	}else{
		var	 data = {};
		console.log("1");
		if( localStorage.getObject("expiry_timestamp") === null ){
			console.log("2");
			getProjectDataV2(projectId);
			data["time"+projectId] =  new Date();
			localStorage.setObject("expiry_timestamp", data);
		}else{
		console.log("3");
			data = localStorage.getObject("expiry_timestamp");
			if(data["time"+projectId] === null || data["time"+projectId] === undefined){
				//get new data
				getProjectDataV2(projectId);
				data["time"+projectId] =  new Date();
				localStorage.setObject("expiry_timestamp", data);
			}else{
				if( DateCompare(new Date(), data["time"+projectId]) == 1 ) {
					//get new data
					getProjectDataV2(projectId);
					data["time"+projectId] =  new Date();
					localStorage.setObject("expiry_timestamp", data);
				}else{
					console.log('updated');
				}
			}
		}
	}
}

function DateCompare(DateA, DateB) {
	var a = new Date(DateA);
	var b = new Date(DateB);

	var msDateA = Date.UTC(a.getFullYear(), a.getMonth()+1, a.getDate());
	var msDateB = Date.UTC(b.getFullYear(), b.getMonth()+1, b.getDate());

	if (parseFloat(msDateA) < parseFloat(msDateB))
		return -1;  // less than
	else if (parseFloat(msDateA) == parseFloat(msDateB))
		return 0;  // equal
	else if (parseFloat(msDateA) > parseFloat(msDateB))
		return 1;  // greater than
	else
		return null;  // error
  }

function generateContent(contentData){
	console.log("Project");
	console.log(contentData.project);

	console.log("Items");
	console.log(contentData.items);

	$("body").removeClass("noscroll");
	// $("#global-loader").hide();

	$("#tbl-list").show();
	$("#channel-info-panel").show();
	$("#panel-action-project").show();

	$("#project-new-name").val($("#sel-list option:selected").text());

	//Content
	$("body").removeClass("noscroll");
	// $("#global-loader").hide();

	$("#tbl-list").show();
	$("#channel-info-panel").show();
	$("#panel-action-project").show();

	$("#project-new-name").val($("#sel-list option:selected").text());

	$("#channel-tp1").val(contentData.project.tp1);
	$("#channel-tp2").val(contentData.project.tp2);
	$("#channel-tp3").val(contentData.project.tp3);
	$("#channel-offer-url").val(contentData.project.offer);
	$("#channel-sale-profit").val(contentData.project.sale_profit);
	$("#channel-cost-rate").val(contentData.project.cost_rate);
	$("#channel-combined-average-view").html(contentData.project.combine_ave);
	$("#channel-cost").html(contentData.project.combine_cost);
	$("#estimated-sales").html(contentData.project.estemated_sales+"");
	$("#estimated-gross").html(contentData.project.estemated_gross+"");
	$("#estimated-net").html(contentData.project.estemated_net+"");
	//end of content

	// test


	//test


	//genrate table contents
	contentData.items.forEach(function(item){
		var tr = $("<tr id="+item.id+"></tr>");
		tr.append('<td><label class="checkbox-inline"><input type="checkbox" class="check-item" data-check-id="'+item.id+'" style="margin-left:-12px;" value=""></label></td>');
		tr.append('<td><img src="'+item.img+'" width="50" /></td>');
		tr.append('<td><a href="'+base_url+'dashboard/view/'+item.channelId+'" >'+item.title+'</a></td>');
		if(item.email != "")
			var email = item.email;
		else
			var email = "none";


		if(item.is_active == "1"){
			var ave_view_num = "";
			var is_active = "checked";
			var active_number = 1;
		}
		else{
			var ave_view_num = '';
			var is_active = "";
			var active_number = 2;
		}

		tr.append('<td>'+email+'</td>');
		tr.append('<td>'+item.video+'</td>');
		tr.append('<td><span id="ave-view-num-container-'+item.id+'" style="'+ave_view_num+'" >'+item.ave_view+'</span></td>');
		tr.append('<td>'+item.cost+'</td>');
		tr.append('<td><button class="btn btn-danger" onclick="deleteItem(\''+item.id+'\',this)" ><span class="glyphicon glyphicon-trash" ></span></button></td>');
		tr.append('<td><div class="col-md-4"><input id="sw'+item.id+'" type="checkbox" name="my-checkbox" class="button-checkbox" data-item-id="'+item.id+'" data-on-text="On" data-off-text="Off" '+is_active+' ></div></td>');
		tr.append('<td id="state'+item.id+'" class="hidden">'+active_number+'</td>');
		tr.append('<td>'+item.video+'</td>');
		tr.append('<td>'+item.ave_view+'</td>');
		tr.append('<td>'+item.cost+'</td>');
		$("#tbl-list-body").append(tr);
	});

	// init bootstrapswitch
	$('.button-checkbox').bootstrapSwitch();
	$('.button-checkbox').on('switchChange.bootstrapSwitch', function (event, state) { 
		var state = $(this);

		if( $("#state"+state.data("item-id") ).html() == "1" ){
			$("#state"+state.data("item-id") ).html("2");
		}else{
			$("#state"+state.data("item-id") ).html("1"); 
		}
	});

	//init switch bootstrap 
	$('.button-checkbox').on('switchChange.bootstrapSwitch', function (event, state) {
		if(!switch_all_checked_item)
		{
			var is_active = $(this).prop("checked");
			var item_id = $(this).data('item-id');
			$("#channel-info-loader").show();

			$(".button-checkbox").bootstrapSwitch('disabled',true);
			$.ajax({
				url:base_url+'project/projectUpdateItem',
				type:'post',
				data:{is_active:is_active,item_id:item_id},
				dataType:'json',
				success:function(data){
					$(".button-checkbox").bootstrapSwitch('disabled',false);
					$("#channel-info-loader").hide();
					$("#channel-combined-average-view").html(data.combine_ave);
					$("#channel-cost").html(data.combine_cost);
					$("#estimated-sales").html(data.estemated_sales+"");
					$("#estimated-gross").html(data.estemated_gross+"");
					$("#estimated-net").html(data.estemated_net+"");
				}
			});
		}
	});

	//init datatable 
	table_channel = $("#tbl-list").DataTable({
	"lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],
	'columnDefs': [
	    { 'orderData':[9], 'targets': [8] },
	    {"targets":7,"orderable":false}
	],
	"initComplete": function( settings, json ) {
		var select = $('<select class="form-control" id="sel-bulk-action" name="method" style="display: inline-block;width: 60%;margin-right: 5px;" ></select>');
		select.append('<option value="default" >--</option>');
		select.append('<option value="update" >Update Data</option>');
		select.append('<option value="activate" >Activate</option>');
		select.append('<option value="deactivate" >Deactivate</option>');

		var div_col_3 = $('<div class="col-md-3" ></div>');
		div_col_3.append(select);
		var button_go = $('<button class="btn btn-default" >Go</button>');

		div_col_3.append(button_go);

		var parent_div = $('<div class="select-custom-contaner" ></div>');
		var form = $('<form action="'+base_url+'project/UpdateBulkItems" method="post" target="load-bulk-channel">');

		form.submit(function(){
			
			if($(".check-item:checked").length != 0)
			{
				$("body").addClass("noscroll");
				$("#global-loader").show();
				
				var method = $("#sel-bulk-action").val();
				switch_all_checked_item = true;
				var channel_id_arr = [];
				var count = 0;
				$(".check-item:checked").each(function(){

					if(method == "activate")
					{
						$('.button-checkbox[data-item-id="'+$(this).data('check-id')+'"]').bootstrapSwitch('state', true);
					}
					if(method == "deactivate")
					{
						$('.button-checkbox[data-item-id="'+$(this).data('check-id')+'"]').bootstrapSwitch('state', false);
					}

					channel_id_arr.push($(this).data('check-id'));
					count++;

					if(count == $(".check-item:checked").length)
					{
						switch_all_checked_item = false;
						$("#channel_ids").val(JSON.stringify(channel_id_arr));
					}

				});
				setTimeout(function(){
					$("#load-bulk-channel").show();
				},2000);
				
			}else
			{
				return false;
			}
		});

		var channel_arr_input = $('<input type="hidden" name="channel_ids" id="channel_ids" value="" >');
		var bulk_project_id = $('<input type="hidden" name="projectId" id="projectId" value="'+$("#sel-list").val()+'" >');

		form.append(channel_arr_input);
		form.append(bulk_project_id);
		form.append(div_col_3);

		parent_div.append(form);
		//parent_div.append(div_col_1);
		
		$("#tbl-list_filter").after(parent_div);
		}
	});
	// table_channel.column(9).visible(false);
	table_channel.column(10).visible(false);
	table_channel.column(11).visible(false);
	table_channel.column(12).visible(false);

}