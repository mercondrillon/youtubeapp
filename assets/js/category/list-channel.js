$(document).ready(function(){
	// $("#category-channel-list").dataTable( {
	//     "paging": false,
	//     "aaSorting":[[1,'desc']]
	// } );

		datatable_channel = $('#category-channel-list').dataTable( {
		    "pageLength": 200,
	     	"aaSorting":[[3,'desc']],
		    "columnDefs": [
		        {
		            "targets": [ 3 ],
		            "visible": false,
		            "searchable": false
		        },
		         {
		            "targets": [ 2 ],
		            "orderable": false
		        },
		        {
	            	"targets": 0,
	            	"render": function ( data, type, row, meta ) {
	            		
	                	var itemID = row.id;

	                	// if(notie_status)
	                	// {
	                	// 	notie_status = false;
	                	// 	notie.alert({type:'success',text:'load result',time:1});
	                	// }
	                	
	                	//var project = '<div class="dropdown pull-right"><button class="btn btn-primary dropdown-toggle " onclick="loadProject(\''+itemID+'\',this);" type="button" data-toggle="dropdown">Add to Project <span class="caret"></span></button><ul class="dropdown-menu"><li class="project-spinner"><a href="javascript:void(0);" style="text-align: center;"><i class="fa fa-spinner fa-spin" aria-hidden="true" ></i></a></li></ul></div>';                   
	                	return '<a href="'+base_url+'dashboard/view/' + itemID + '" target="_blank" >' + data + '</a> ';
	            	}
	            },
	            {
	            	"targets": 1,
	            	"render": function ( data, type, row, meta ) {
	            		
	                	if(data == "")
	                	{
	                		return "none";
	                	}else
	                	{
	                		return data;
	                	}
	                	
	            	}
	            },
	            {
	            	"targets": 2,
	            	"render": function ( data, type, row, meta ) {

	            		var itemID = row.id;

	                	var project = '<div class="dropdown pull-right"><button class="btn btn-primary dropdown-toggle " onclick="loadProject(\''+itemID+'\',this);" type="button" data-toggle="dropdown">Add to Project <span class="caret"></span></button><ul class="dropdown-menu"><li class="project-spinner"><a href="javascript:void(0);" style="text-align: center;"><i class="fa fa-spinner fa-spin" aria-hidden="true" ></i></a></li></ul></div>';                   
	                	project += '<button class="btn btn-danger" onclick="archiveChannel(\''+itemID+'\',this);" >Archive</button>';
	                	return project;
	                	
	            	}
	            }
	        ],
	        "bProcessing": true,
			"bServerSide": true,
			"sAjaxSource": base_url+"dashboard/get_channels",
			"fnServerParams": function ( aoData ) {
	            aoData.push( { "name": "category", "value": $("#category-id").val() } );
	            aoData.push( { "name": "table_data", "value": ['title','email','','subscriberCount','aveViews'] } );
	        },
	        "columns": [
	            { "data": "title" },
	            { "data": "email" },
	            { "data": "email" },
	            { "data": "subscriberCount" },
	            { "data": "ave_view" },
	        ],
		   
		} );

		$('#category-channel-list_filter input').unbind();
		$('#category-channel-list_filter input').bind('keyup', function(e) {
	       	if(e.keyCode == 13) {
	       		notie_status = true;
	       		
	        	datatable_channel.fnFilter(this.value);   
	    	}
		}); 
});