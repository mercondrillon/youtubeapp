$(document).ready(function(){
    if( $("#return_projectId").length ){
        if($("#return_projectId").val() != 'default'){
            var id = $("#return_projectId").val();
            if( checkLocalStorage() ){
                $.ajax({
                    url:base_url+'project/getProjectItemsv2',
                    type:"post",
                    data:{id:id},
                    dataType:'json',
                    success:function(data){ 
                        if( id == data.project.id ){
                            localStorage.setObject("project"+data.project.id, data);
                            console.log("Import - setLocal");
                        }
                    }
                });
            }
        }
    }
    
    $("#btn-import-channel").click(function(){
        if( $("#channel-url").val().replace(/\s/g,'') != "") {
            var con = confirm('are you sure to import this links?');

            if(con)
            {
                var channels = $('#channel-url').val().split('\n');
                $("#bulk-channel-value").val(JSON.stringify(channels));
                $("#loading-icon").show();
                $("#project-id").val($("#sle-project").val());
                $("#form-import-bulk-channel").submit();
                $("#load-import-bulk-channel").load(function(){
                    $("#loading-icon").hide();
                });
            }
        }
    });
});
