$(document).ready(function(){
	

	$("#btn-save-new-project").click(function(){

		$("#container-response-new-project").html('');

		var title = $("#new-project-title").val();

		if(title.replace(/\s/g,'') == "")
		{
			var msg = '<div class="alert alert-danger"><strong>Error</strong> The title for the Project is required.</div>';
			$("#container-response-new-project").html(msg);
			return false;
		}

		$.ajax({
			url:base_url+'project/add',
			type:"post",
			data:{title:title},
			success:function(data){
				var msg = '<div class="alert alert-success"><strong>Success!</strong> New Project Added.</div>';
				$("#container-response-new-project").html(msg);
				$("#new-project-title").val('');
			}
		});
	});


	$('#modal-create-project').on('hidden.bs.modal', function () {
	    $("#container-response-new-project").html('');
	});
});

function loadProject(channelId,btn){


	var parent = $(btn).parent();
	parent.find(".dropdown-menu").find('li').each(function(){
		if(!$(this).hasClass('project-spinner'))
		{
			$(this).remove();
		}
	});
	parent.find(".dropdown-menu").find(".project-spinner").show();

	$.ajax({
		url:base_url+'project/getList',
		type:'post',
		data:{channelId:channelId},
		dataType:'json',
		success:function(data)
		{
			parent.find(".dropdown-menu").find(".project-spinner").hide();

			if(data.projects)
			{
				data.projects.forEach(function(list){
					var disable = $('<a href="javascript:addToProject(\''+list.id+'\',\''+channelId+'\')">'+list.title+'</a>');
					disable.click(function(e){
						e.stopPropagation();
					});
					if(list.in_list)
						disable = $('<a href="javascript:void(0)">'+list.title+'</a>');

					var li = $("<li></li>");
					li.append(disable);

					parent.find(".dropdown-menu").append(li);
				});
			}else
			{
				parent.find(".dropdown-menu").append('<li><a href="javascript:void(0)">No Project</a></li>');
			}
		}
	});
}

function addToProject(projectId,channelId)
{

	notie.alert({ type: "warning", text: 'Adding to the project ...', stay:true });

	$.ajax({
		url:base_url+'project/addItem',
		type:"post",
		data:{projectId:projectId,channelId:channelId},
		dataType:'json',
		success:function(data){
			notie.alert({ type: "success", text: 'New channel added in '+data.title, time:3 });
		}
	});


	// Add this later to main js and remove from other scripts.
	if (Modernizr.localstorage) {
	  	$.ajax({
			url:base_url+'project/getProjectItemsv2',
			type:"post",
			data:{id:projectId},
			dataType:'json',
			success:function(data){ 
				if( projectId == data.project.id ){
					localStorage.setObject("project"+data.project.id, data);
					console.log("getProjectDataV2 - setLocal");
				}
			}
		});
	} else {
		return false;
	  	// localStorage not supported
	}

}

function archiveChannel(itemId,btn)
{
	notie.alert({ type: "warning", text: 'Updating status ...', stay:true });

	$.ajax({
		url:base_url+'channel/archiveChannel',
		type:"post",
		data:{itemId:itemId},
		dataType:'json',
		success:function(data){
			$(btn).parent().parent().remove();
			notie.alert({ type: "success", text: 'Channel is now archive', time:3 });
		}
	});
}

//start of local functions 
Storage.prototype.setObject = function(key, value) {
    this.setItem(key, JSON.stringify(value));
}

Storage.prototype.getObject = function(key) {
    var value = this.getItem(key);
    return value && JSON.parse(value);
}

//check if html storage name exist
function checkLocal(projectId){
	if( localStorage.getItem("project"+projectId) === null){
		return false;
	}else{
		return true;
	}
}

// Check if localstorage is available
function checkLocalStorage(){
	if (Modernizr.localstorage) {
	  	// localStorage is supported
		return true;
	} else {
		return false;
	  	// localStorage not supported
	}
}
