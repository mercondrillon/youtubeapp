	var notie_status = false;
	var first_load = true;
	$(document).ready(function(){
		datatable_channel = $('#tbl-channels-list').dataTable( {
		    "pageLength": 200,
	     	"aaSorting":[[3,'desc']],
		    "columnDefs": [
		        {
		            "targets": [ 3 ],
		            "visible": false,
		            "searchable": false
		        },
		         {
		            "targets": [ 2 ],
		            "orderable": false
		        },
		        {
	            	"targets": 0,
	            	"render": function ( data, type, row, meta ) {
	            		
	                	var itemID = row.id;

	                	// if(notie_status)
	                	// {
	                	// 	notie_status = false;
	                	// 	notie.alert({type:'success',text:'load result',time:1});
	                	// }
	                	
	                	//var project = '<div class="dropdown pull-right"><button class="btn btn-primary dropdown-toggle " onclick="loadProject(\''+itemID+'\',this);" type="button" data-toggle="dropdown">Add to Project <span class="caret"></span></button><ul class="dropdown-menu"><li class="project-spinner"><a href="javascript:void(0);" style="text-align: center;"><i class="fa fa-spinner fa-spin" aria-hidden="true" ></i></a></li></ul></div>';                   
	                	return '<a href="'+base_url+'dashboard/view/' + itemID + '" target="_blank" >' + data + '</a> ';
	            	}
	            },
	            {
	            	"targets": 1,
	            	"render": function ( data, type, row, meta ) {
	            		
	                	if(data == "")
	                	{
	                		return "none";
	                	}else
	                	{
	                		return data;
	                	}
	                	
	            	}
	            },
	            {
	            	"targets": 2,
	            	"render": function ( data, type, row, meta ) {

	            		var itemID = row.id;

	                	var project = '<div class="dropdown pull-right"><button class="btn btn-sm btn-primary dropdown-toggle " onclick="loadProject(\''+itemID+'\',this);" type="button" data-toggle="dropdown">Add to Project <span class="caret"></span></button><ul class="dropdown-menu"><li class="project-spinner"><a href="javascript:void(0);" style="text-align: center;"><i class="fa fa-spinner fa-spin" aria-hidden="true" ></i></a></li></ul></div>';                   
	                	project += '<button class="btn btn-sm btn-danger" onclick="archiveChannel(\''+itemID+'\',this);" >Archive</button>';
	                	return project;
	                	
	            	}
	            }
	        ],
	        "bProcessing": true,
			"bServerSide": true,
			"sAjaxSource": base_url+"dashboard/get_channels",
			"fnServerParams": function ( aoData ) {
	            aoData.push( { "name": "table_data", "value": ['title','email','','subscriberCount','aveViews'] } );
	            aoData.push({"name":"first_load","value":first_load});
	        },
	        "columns": [
	            { "data": "title" },
	            { "data": "email" },
	            { "data": "email" },
	            { "data": "subscriberCount" },
	            { "data": "ave_view" },
	        ],
		   
		} );

		$("#filter-category").change(function(){
			var oTable = $('#tbl-channels-list').dataTable();
  			oTable.fnPageChange( 'first' );
			
		});
		
	    $("#btn-sync").click(function(){
	    	$("#channel-loader").show();
	    	$.ajax({
				type:'get',
				url:base_url+"channel/get_data_top_5000",
				success:function(data){
					var channel_data = [];
					var parent = $(data);
					parent.find('.TableMonthlyStats').each(function(){
						if($(this).find('a').length != 0)
						{
							var url = $(this).find('a').attr('href').split('/')[$(this).find('a').attr('href').split('/').length - 1];
							var name = $(this).find('a').html();

							channel_data.push([url,name]);
						}
					});
					
					$(".channels").val(JSON.stringify(channel_data));
					$(".form-sync-to-load").each(function(){
						$(this).submit();
					});
					
					
				}
			});
	    });

     	$('#tbl-channels-list_filter input').unbind();
		$('#tbl-channels-list_filter input').bind('keyup', function(e) {
	       	if(e.keyCode == 13) {
	       		notie_status = true;
	       		
	        	datatable_channel.fnFilter(this.value);   
	    	}
		}); 
	});