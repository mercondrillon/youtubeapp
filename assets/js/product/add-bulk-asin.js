$(document).ready(function(){

	$("#form-add-bulk-asin").submit(function(){
		if($("#asin-bulk").val().replace(/\s/g,'') != "")
        {
        	var con = confirm('are you sure to import this ASIN?');

            if(con)
            {
                var asin = $('#asin-bulk').val().split('\n');
                $("#asin").val(JSON.stringify(asin));
               
            }else
            {
            	return false;
            }
        }else
        {
        	return false;
        }
	});

});