$(document).ready(function(){

	$("#asin-search-in").submit(function(){
		var asin = $("#ASIN").val();

		if(asin.replace(/\s/g,'') == "")
		{
			alert('Invalid ASIN');
			return false;

		}
	});

	$(".btn-delete-prod").click(function(){
		var prod_id = $(this).data('prod-id');
		var btn = $(this);
		notie.confirm({
		  text: "Are you sure to delete this Product?",
		  submitText: "Delete", // optional, default = 'Yes'
		  cancelText: 'Cancel', // optional, default = 'Cancel'
		  submitCallback: function(){
		  	$.ajax({
		  		type:'post',
		  		url:base_url+'product/deleteProduct',
		  		data:{prod_id:prod_id},
		  		success:function(data){
		  			btn.parent().parent().remove();
		  		}
		  	});
		  }
		});
	});

	$("#table-product").dataTable();
});