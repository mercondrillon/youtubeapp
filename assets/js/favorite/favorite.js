function favorite(channel,user,btn)
{
	$(btn).addClass('disabled');
	$.ajax({
		"url":base_url+'favorite/channel',
		'type':'post',
		data:{channel:channel,user:user},
		success:function(data)
		{
			$(btn).parent().parent().remove();
			
			var favorite_num = parseInt($(".user-badge-favorite").html());
			$(".user-badge-favorite").html(favorite_num-1);
			
				
		},
	});
	
}
$(document).ready(function(){
		datatable_channel = $('#favorite-channel-list').dataTable( {
		    "pageLength": 200,
	     	"aaSorting":[[0,'asc']],
		    "columnDefs": [
		        {
		            "targets": [ 2 ],
		            "orderable": false,
		            
		        },
		        {
	            	"targets": 0,
	            	"render": function ( data, type, row, meta ) {
	            		
	                	var itemID = row.id;

	                	// if(notie_status)
	                	// {
	                	// 	notie_status = false;
	                	// 	notie.alert({type:'success',text:'load result',time:1});
	                	// }
	                	
	                	//var project = '<div class="dropdown pull-right"><button class="btn btn-primary dropdown-toggle " onclick="loadProject(\''+itemID+'\',this);" type="button" data-toggle="dropdown">Add to Project <span class="caret"></span></button><ul class="dropdown-menu"><li class="project-spinner"><a href="javascript:void(0);" style="text-align: center;"><i class="fa fa-spinner fa-spin" aria-hidden="true" ></i></a></li></ul></div>';                   
	                	var favor = '<button class="btn btn-primary" onclick="favorite(\''+itemID+'\',\''+$("#user-id").val()+'\',this)" ><span class="glyphicon glyphicon-thumbs-up"></span></button>';
	                	return '<a href="'+base_url+'dashboard/view/' + itemID + '" target="_blank" >' + data + '</a> '+favor;
	            	}
	            },
	            {
	            	"targets": 1,
	            	"render": function ( data, type, row, meta ) {
	            		
	                	if(data == "")
	                	{
	                		return "none";
	                	}else
	                	{
	                		return data;
	                	}
	                	
	            	}
	            },
	            {
	            	"targets": 2,
	            	"render": function ( data, type, row, meta ) {
	            		
	                	return "<img src='"+JSON.parse(data).default.url+"' />";
	                	
	            	}
	            }
	        ],
	        "bProcessing": true,
			"bServerSide": true,
			"sAjaxSource": base_url+"dashboard/get_channels",
			"fnServerParams": function ( aoData ) {
	            aoData.push( { "name": "favorite", "value": true } );
	            aoData.push( { "name": "table_data", "value": ['title','email','thumbnails','videoCount','aveViews'] } );
	        },
	        "columns": [
	            { "data": "title" },
	            { "data": "email" },
	            { "data": "thumbnails" },
	            { "data": "videoCount" },
	            { "data": "ave_view" },
	        ],
		   
		} );

		$('#favorite-channel-list_filter input').unbind();
		$('#favorite-channel-list_filter input').bind('keyup', function(e) {
	       	if(e.keyCode == 13) {
	       		notie_status = true;
	       		
	        	datatable_channel.fnFilter(this.value);   
	    	}
		}); 
});