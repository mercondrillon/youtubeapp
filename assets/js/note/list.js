$(document).ready(function(){
	$(".save-user-note").click(function(){

		var index = $(this).data('index');
		var id = $("#note-id-"+index).val();
		var note = $("#user-notes-"+index).val();
		var channel = 0;
		var url = base_url+"note/add";
		if(id != 0)
			url = base_url+"note/update";

		$("#global-loader").show();
		$("body").addClass('noscroll');

		$.ajax({
			url:url,
			type:'post',
			data:{id:id,note:note,channel:channel},
			success:function(data){
				$("#global-loader").hide();
				$("body").removeClass('noscroll');
				$("#note-id").val(data);
				var msg = '<div class="alert alert-success"><strong>Success!</strong> Note successfully saved.</div>';
				$(".note-msg-container-"+index).html(msg);

				var note_num = parseInt($(".user-badge-note").html());
				if(url == (	base_url+"note/add")){
					$(".user-badge-note").html(note_num+1);
				}else
				{
					if($("#pre-"+index).length != 0)
					{
						$("#pre-"+index).html(note);
					}
				}
			}
		});


	});

	$(".delete-user-note").click(function(){

		var con = confirm('area you sure to delete this note?');

		if(con)
		{

			var index = $(this).data('index');
			var id = $("#note-id-"+index).val();
			var btn = $(this);
			$.ajax({
				url:base_url+'note/delete',
				type:'post',
				data:{id:id},
				success:function(data){
					btn.parent().parent().remove();
					var note_num = parseInt($(".user-badge-note").html());
					$(".user-badge-note").html(note_num-1);
				}
			});	
		}
		
	});

	$("#tbl-note").dataTable();
});