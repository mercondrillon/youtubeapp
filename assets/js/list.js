var table_channel;
var switch_all_checked_item = false;
$(document).ready(function(){
	$("#tbl-list").hide();

	$("#sel-list").change(function(){
		var id = $(this).val();
		if ( $.fn.DataTable.isDataTable( '#tbl-list' ) ) {
  			table_channel.destroy();
		}
		
		$("#tbl-list").hide();
		$("#channel-info-panel").hide();
		$("#panel-action-project").hide();
		$("#tbl-list-body").empty();

		

		if(id != "default")
		{
			$("body").addClass("noscroll");
			$("#global-loader").show();
			loadProjectData(id);
		}

	});

	$("#btn-edit-project").click(function(){
		var text = $('#project-new-name').val();
		var projectId = $("#sel-list").val();
		if(text.replace(/\s/g,'') != "")
		{
			
			$.ajax({
				url:base_url+'project/edit',
				type:'post',
				data:{projectId:projectId,text:text},
				success:function(data){
					
					$('#edit-project-name').modal('hide');
					$("#sel-list option:selected").text(text);
					notie.alert({type:'success',text:'Project successfully saved!',time:2});
				}
			});

		}else
		{
			notie.alert({type:'error',text:'The project name is empty',time:2});
		}
		
	});

	$("#btn-delete-project").click(function(){
		notie.confirm({
			text: 'Are you sure to delete this project?',
			submitCallback:function(){
				notie.alert({type:'warning',text:'Deleting project ...',stay:true});
				var id = $("#sel-list option:selected").val();
				$("#channel-info-loader").show();
				$.ajax({
					url:base_url+'project/deleteProject',
					type:'post',
					data:{id:id},
					success:function(data){
						$("#channel-info-loader").hide();
						notie.alert({type:'success',text:'Project successfully deleted',time:2});
						$("#tbl-list").hide();
						$("#channel-info-panel").hide();
						$("#panel-action-project").hide();
						$("#tbl-list-body").empty();
						$("#sel-list option:selected").remove();
						
					}
				});
			}
		});	
	});

	$("#btn-save-project-info").click(function(){
		var tp1 = $("#channel-tp1").val();
		var tp2 = $("#channel-tp2").val();
		var tp3 = $("#channel-tp3").val();
		var url = $("#channel-offer-url").val();
		var cost_rate = $("#channel-cost-rate").val();
		var sale_profit = $("#channel-sale-profit").val();
		var projectId = $("#sel-list").val();
		notie.alert({type:'warning',text:'Saving...',stay:true});
		$("#channel-info-loader").show();
		$.ajax({
			url:base_url+'project/edit',
			type:'post',
			data:{
				tp1:tp1,
				tp2:tp2,
				tp3:tp3,
				url:url,
				cost_rate:cost_rate,
				sale_profit:sale_profit,
				option:'info',
				projectId:projectId
			},
			dataType:'json',
			success:function(data){
				$("#channel-info-loader").hide();
				$("#estimated-sales").html(data.estemated_sales+"");
				$("#estimated-gross").html(data.estemated_gross+"");
				$("#estimated-net").html(data.estemated_net+"");
				notie.alert({type:'success',text:'Project successfully saved.',time:3});
			}

		});

	});

	$("#btn-duplicate-project").click(function(){

		var projectId = $("#sel-list").val();
		notie.confirm({
			text:'Are you sure to duplicate this project?',
			submitCallback:function(){
				notie.alert({type:'warning',text:'Duplicating...',stay:true});
				$.ajax({
					url:base_url+'project/duplicateProject',
					type:'post',
					data:{projectId:projectId},
					dataType:'json',
					success:function(data)
					{

						notie.alert({type:'success',text:'successfully duplicate project',time:3});
						var option = $("<option value='"+data.id+"' >"+data.name+"</option>");
						 $("#sel-list").append(option);
					}

				});
			}
		});
	});

	$("#btn-refresh-project").click(function(){
		notie.alert({type:'warning',text:'Updating channel data in this project...',stay:true});
		$("body").addClass("noscroll");
		$("#global-loader").show();

		var projectId = $("#sel-list").val();

		$.ajax({
			type:'post',
			url:base_url+'channel/refreshProjectChannel',
			data:{projectId:projectId},
			dataType:'json',
			success:function(data){
				notie.alert({type:'success',text:'successfully update project channel',time:3});

				if ( $.fn.DataTable.isDataTable( '#tbl-list' ) ) {
		  			table_channel.destroy();
				}
				$("#tbl-list-body").empty();

				loadProjectData(projectId);
			}
		});
	});

	$("#btn-export-project").click(function(){
		notie.confirm({
			text:'Are you sure to export this project?',
			submitCallback:function(){
				$("#global-loader").show();
				$("body").addClass('noscroll');
				var projectId = $("#sel-list").val();
				$.ajax({
					url:base_url+'project/exportProject',
					type:'post',
					data:{projectId:projectId},
					success:function(data)
					{
						$("#global-loader").hide();
						$("body").removeClass('noscroll');
						$("#download-csv-project").attr('src',base_url+'project/downloadCsv/'+data);
					}

				});
			}
		});
		

	});

	$(".parent-item-check").click(function(){
		var parent = $(this).prop( "checked" );
		$(".check-item").each(function(){
			$(this).prop('checked', parent);			
		});
	});

	$("#load-bulk-channel").load(function(){
		var id = $("#sel-list").val();
		$("#load-bulk-channel").hide();
		$("#load-computation-p").show();
		$.ajax({
			url:base_url+'project/getProjectItems',
			type:"post",
			data:{id:id},
			dataType:'json',
			success:function(data){
				$("#load-computation-p").hide();
				$("#channel-combined-average-view").html(data.project.combine_ave);
				$("#channel-cost").html(data.project.combine_cost);
				$("#estimated-sales").html(data.project.estemated_sales+"");
				$("#estimated-gross").html(data.project.estemated_gross+"");
				$("#estimated-net").html(data.project.estemated_net+"");
				$("body").removeClass("noscroll");
				$("#global-loader").hide();
				$("#load-bulk-channel").hide();
				$("#load-computation-p").hide();
				
				//$("#load-bulk-channel").attr('src','about:blank');
			}
		});
		
	});

	$("#btn-share-project").click(function(){
		notie.alert({type:'warning',text:'Creating share link for this project',stay:true});
		
		$.ajax({
			url:base_url+"project/getShareLink",
			type:'post',
			data:{projectId:$("#sel-list").val()},
			success:function(link){
				notie.input({text:'Shareable link',submitText:'Copy to Clipboard',cancelText:'Close',value:link,submitCallback:function(value){
					// var input_hidden = $('<input type="hidden" value="'+value+'" id="input-hidden-link" >');
					// $("body").append(input_hidden);
					// $(".notie-input-field").focus();
					// succeed = document.execCommand("copy");
					//$("#input-hidden-link").remove();
				}});
				$(".notie-input-field").attr('id','input-share-link');
				$(".notie-background-success").attr('data-clipboard-target','#input-share-link');
				var clipboard = new Clipboard('.notie-background-success');
				//Clipboard('.notie-background-success');
				
			}
		});

	});

});

function deleteItem(itemId,btn)
{
	var parent = $(btn).parent().parent();
	notie.confirm({
		text:'Are to delte this items',
		submitCallback:function(){
			$.ajax({
				url:base_url+'project/deleteItem',
				type:'post',
				data:{itemId:itemId},
				dataType:'json',
				success:function(data){

					$("#channel-combined-average-view").html(data.combine_ave);
					$("#channel-cost").html(data.combine_cost);
					$("#estimated-sales").html(data.estemated_sales+"");
					$("#estimated-gross").html(data.estemated_gross+"");
					$("#estimated-net").html(data.estemated_net+"");
					parent.remove();
				}
			});
		}
	});
	
}


function loadProjectData(id)
{
	$(".parent-item-check").prop('checked',false);
	$.ajax({
		url:base_url+'project/getProjectItems',
		type:"post",
		data:{id:id},
		dataType:'json',
		success:function(data){
			
			$("body").removeClass("noscroll");
			$("#global-loader").hide();

			$("#tbl-list").show();
			$("#channel-info-panel").show();
			$("#panel-action-project").show();

			$("#project-new-name").val($("#sel-list option:selected").text());

			$("#channel-tp1").val(data.project.tp1);
			$("#channel-tp2").val(data.project.tp2);
			$("#channel-tp3").val(data.project.tp3);
			$("#channel-offer-url").val(data.project.offer);
			$("#channel-sale-profit").val(data.project.sale_profit);
			$("#channel-cost-rate").val(data.project.cost_rate);
			$("#channel-combined-average-view").html(data.project.combine_ave);
			$("#channel-cost").html(data.project.combine_cost);
			$("#estimated-sales").html(data.project.estemated_sales+"");
			$("#estimated-gross").html(data.project.estemated_gross+"");
			$("#estimated-net").html(data.project.estemated_net+"");
			
			
			data.items.forEach(function(item){
				var tr = $("<tr></tr>");
				tr.append('<td><label class="checkbox-inline"><input type="checkbox" class="check-item" data-check-id="'+item.id+'" style="margin: 11px;margin-left:19px;" value=""></label></td>');
				tr.append('<td><a href="'+base_url+'dashboard/view/'+item.channelId+'" >'+item.title+'</a></td>');
				if(item.email != "")
					var email = item.email;
				else
					var email = "none";


				if(item.is_active == "1"){
					var ave_view_num = "";
					var is_active = "checked";
					var active_number = 1;
				}
				else{
					var ave_view_num = '';
					var is_active = "";
					var active_number = 2;
				}
			

				
				tr.append('<td>'+email+'</td>');
				tr.append('<td><img src="'+item.img+'" /></td>');
				tr.append('<td>'+item.video+'</td>');
				tr.append('<td><span id="ave-view-num-container-'+item.id+'" style="'+ave_view_num+'" >'+item.ave_view+'</span></td>');
				tr.append('<td>'+item.cost+'</td>');
				tr.append('<td><button class="btn btn-danger" onclick="deleteItem(\''+item.id+'\',this)" ><span class="glyphicon glyphicon-trash" ></span></button></td>');
				tr.append('<td><div class="col-md-4"><input type="checkbox" name="my-checkbox" class="button-checkbox" data-item-id="'+item.id+'" data-on-text="active" data-off-text="deactivate" '+is_active+' ></div></td>');
				tr.append('<td>'+active_number+'</td>');
				$("#tbl-list-body").append(tr);
				
			});

			$('.button-checkbox').bootstrapSwitch();
			$('.button-checkbox').on('switchChange.bootstrapSwitch', function (event, state) {

				if(!switch_all_checked_item)
				{
					var is_active = $(this).prop("checked");
					var item_id = $(this).data('item-id');
					$("#channel-info-loader").show();
					// $("#global-loader").show();
					// $("body").addClass('noscroll');

					$(".button-checkbox").bootstrapSwitch('disabled',true);
					$.ajax({
						url:base_url+'project/projectUpdateItem',
						type:'post',
						data:{is_active:is_active,item_id:item_id},
						dataType:'json',
						success:function(data){
							$(".button-checkbox").bootstrapSwitch('disabled',false);
							// $("#global-loader").hide();
							// $("body").removeClass('noscroll');
							// if(!$("#ave-view-num-container-"+item_id).is(':visible'))
							// 	$("#ave-view-num-container-"+item_id).show();
							// else
							// 	$("#ave-view-num-container-"+item_id).hide();

							$("#channel-info-loader").hide();
							$("#channel-combined-average-view").html(data.combine_ave);
							$("#channel-cost").html(data.combine_cost);
							$("#estimated-sales").html(data.estemated_sales+"");
							$("#estimated-gross").html(data.estemated_gross+"");
							$("#estimated-net").html(data.estemated_net+"");
						}
					});
				}
				
			}); 
			

			table_channel = $("#tbl-list").DataTable({
				"lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],
				'columnDefs': [
				    { 'orderData':[9], 'targets': [8] },
				    {"targets":7,"orderable":false}
				],
				"initComplete": function( settings, json ) {
    				var select = $('<select class="form-control" id="sel-bulk-action" name="method" style="display: inline-block;width: 60%;margin-right: 5px;" ></select>');
    				select.append('<option value="default" >--</option>');
    				select.append('<option value="update" >Update Data</option>');
    				select.append('<option value="activate" >Activate</option>');
    				select.append('<option value="deactivate" >Deactivate</option>');

    				var div_col_3 = $('<div class="col-md-3" ></div>');
    				div_col_3.append(select);
    				var button_go = $('<button class="btn btn-default" >Go</button>');

    				// button_go.click(function(){
    				// 	var method = $("#sel-bulk-action").val();

    				// 	if(method != "default")
    				// 	{

    				// 		if($(".check-item:checked").length != 0)
    				// 		{
    				// 			switch_all_checked_item = true;
    				// 			var channel_id_arr = [];
    				// 			var count = 0;
    				// 			$(".check-item:checked").each(function(){

    				// 				if(method == "activate")
    				// 				{
    				// 					$('.button-checkbox[data-item-id="'+$(this).data('check-id')+'"]').bootstrapSwitch('state', true);
    				// 				}
    				// 				if(method == "deactivate")
    				// 				{
    				// 					$('.button-checkbox[data-item-id="'+$(this).data('check-id')+'"]').bootstrapSwitch('state', false);
    				// 				}
    								
    				// 				channel_id_arr.push($(this).data('check-id'));
    				// 				count++;
    				// 				if(count == $(".check-item:checked").length)
    				// 				{
    				// 					var projectId = $("#sel-list").val();
    				// 					$("body").addClass("noscroll");
								// 		$("#global-loader").show();
    				// 					$.ajax({
    				// 						url:base_url+'project/UpdateBulkItems',
    				// 						type:'post',
    				// 						data:{'channel_ids':JSON.stringify(channel_id_arr),method:method,projectId:projectId},
    				// 						dataType:'json',
    				// 						success:function(data){
    										
    				// 							$("body").removeClass("noscroll");
								// 				$("#global-loader").hide();
								// 				switch_all_checked_item = false;
								// 				$("#channel-combined-average-view").html(data.combine_ave);
								// 				$("#channel-cost").html(data.combine_cost);
								// 				$("#estimated-sales").html(data.estemated_sales+"");
								// 				$("#estimated-gross").html(data.estemated_gross+"");
								// 				$("#estimated-net").html(data.estemated_net+"");
    				// 						}
    				// 					});
    				// 				}
    				// 			});
    				// 		}
    				// 	}
    				// });

    				div_col_3.append(button_go);

    				// var div_col_1 = $('<div class="col-md-1" ></div>');
    				// div_col_1.append($('<button class="btn btn-default" >Go</button>'));

    				var parent_div = $('<div class="select-custom-contaner" ></div>');
    				var form = $('<form action="'+base_url+'project/UpdateBulkItems" method="post" target="load-bulk-channel">');

    				form.submit(function(){
    					
    					if($(".check-item:checked").length != 0)
						{
							$("body").addClass("noscroll");
							$("#global-loader").show();
							
	    					var method = $("#sel-bulk-action").val();
	    					switch_all_checked_item = true;
							var channel_id_arr = [];
							var count = 0;
	    					$(".check-item:checked").each(function(){

	    						if(method == "activate")
								{
									$('.button-checkbox[data-item-id="'+$(this).data('check-id')+'"]').bootstrapSwitch('state', true);
								}
								if(method == "deactivate")
								{
									$('.button-checkbox[data-item-id="'+$(this).data('check-id')+'"]').bootstrapSwitch('state', false);
								}

								channel_id_arr.push($(this).data('check-id'));
								count++;

								if(count == $(".check-item:checked").length)
								{
									switch_all_checked_item = false;
									$("#channel_ids").val(JSON.stringify(channel_id_arr));
								}

	    					});
	    					setTimeout(function(){
	    						$("#load-bulk-channel").show();
	    					},2000);
	    					
	    				}else
	    				{
	    					return false;
	    				}
    				});

    				var channel_arr_input = $('<input type="hidden" name="channel_ids" id="channel_ids" value="" >');
    				var bulk_project_id = $('<input type="hidden" name="projectId" id="projectId" value="'+$("#sel-list").val()+'" >');

    				form.append(channel_arr_input);
    				form.append(bulk_project_id);
    				form.append(div_col_3);

    				parent_div.append(form);
    				//parent_div.append(div_col_1);
    				
    				$("#tbl-list_filter").after(parent_div);
  				}
			});
			table_channel.column(9).visible(false);
		}
	});
}
